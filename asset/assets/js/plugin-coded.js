(function($) {
    //define outside plugin to prevent multiple initialization
    var table = [];
    var keyCount = [];
    var a = 0;
    var test = [];
    var xxuuyy = 0;
    var height = 100;


    $.fn.datalist = function(options) {
        var checkboxid = 0;
        var selected = [];
        var selectedListing = [];
        var selectedListingData = [];
        var listingName = '';
        var xyz = '';
        var current_request = '';



        // Default options
        var settings = $.extend({
            url: 'John Doe',
            offset: 0,
            counter: 0,
            target: 'target',
            start:'',
            end:'',
            courier:'',
            counter: 0,
            region: '',
            member: '',
            dates:'',
            paginate:'',
            report: '',
            user_type: '',
            fresh:'',
            column:'',
            search:'',
            pending:'',
            user_id: '',
            id:'',
            uid: '',
            invoice:'',
            rows:100,
            vendor:'',
            keyword:'',
            result:'',
            guest:''

        }, options);

        var productHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                        +'<th>Products</th>'



        var templateHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                +'<th>Title</th>'
                +'<th>Date</th>'
                +'<th>Watermark</th>'
                 +'<th>Action</th>'

        var salesHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                        +'<th>name</th>'
                        +'<th>name</th>'
                        +'<th>Jan</th>'
                        +'<th>Feb</th>'
                        +'<th>Mar</th>'
                        +'<th>Apr</th>'
                        +'<th>May</th>'
                        +'<th>Jun</th>'
                        +'<th>Jul</th>'
                        +'<th>Aug</th>'
                        +'<th>Sep</th>'
                        +'<th>Oct</th>'
                        +'<th>Nov</th>'
                        +'<th>Dec</th>';


        var stockHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                        +'<th>name</th>'
                        +'<th>available</th>'
                        +'<th>Stock in</th>'
                        +'<th>Return</th>'
                        +'<th>Purchase Return</th>'
                        +'<th>Debit Note</th>'
                        +'<th>Delivery Order</th>'
                        +'<th>Stock Out</th>'
                        +'<th>Cash Sales</th>'
                        +'<th>Online Sales</th>';

        var brandHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                        +'<th>Brand</th>'
                        +'<th>Vendor</th>'
                        +'<th>Action</th>';
        var galleryHeader = '<th><input class="data-check-num" type="checkbox" id="ship_all">'
                        +'<label class="data-check-num" for="ship_all">#</label>'
                        +'</th>'
                        +'<th>Image</th>'
                        +'<th>Video </th>'
                        +'<th>Video Link</th>'
                        +'<th>Action</th>'

        var client = {

            fireAjax: function(offset, paginate, callback, rowCount) {




             if (sessionStorage.getItem("stop_current_loop") != 1) {

                current_request = settings.url;

                $.ajax({
                    url: settings.url,
                    method: "POST",
                    data: {
                        "_token": csrfToken,
                        "offset": (offset ? offset : settings.offset),
                        'pagenumber': settings.counter,
                        'brand_id': settings.brand_id,
                        'sme_id': settings.sme_id,
                        'start': settings.start,
                        'end': settings.end,
                        'courier': settings.courier,
                        'counter': settings.counter,
                        'region': settings.region,
                        'member': settings.member,
                        'id': settings.dates,
                        'paginate': (paginate? paginate : settings.paginate),
                        'report': settings.report,
                        'user_type': settings.user_type,
                        'fresh': settings.fresh,
                        'search': settings.search,
                        'pending': settings.pending,
                        'user_id': settings.user_id,
                        'id': settings.id,
                        'invoice': settings.invoice,
                        'uid': settings.uid,
                        'vendor': settings.vendor,
                        'keyword': settings.keyword,
                        'guest': settings.guest,







                    },
                    dataType: 'json',
                    error: function(msg, b, c) {

                        console.debug('error');
                    },
                    dataType: 'json',
                    success: function(result) {
                        callback(result, rowCount);
                        return false;
                    },
                    type: 'POST'
                });
                 }
    

            },

            /**
             * target DOM element to append the response onto
             * @param  {[type]} response [response from ajax call to be appended]
             * @return {[type]}          [modified DOM]
             */
            target: function(response, keyCount) {
                //uncomment for websocket method
                // response = data.result;
                    
                var activeTab = '';
                var headerContent = productHeader;

                 if (sessionStorage.getItem("stop_current_loop") != 1) {

                    $('#totalRows').text(response['totalRows']);
                    $('#totalRows2').text(response['totalRows']);
                 }
                

                 if (response['brands']) {

                    if (response['report'] == 'sales') {
                        headerContent =  salesHeader;
                    }

                     if (response['report'] == 'stock') {
                        headerContent =  stockHeader;
                    }

                    if (response['report'] == 'template') {
                        headerContent =  templateHeader;

                    }
                     if (response['report'] == 'brand') {
                        headerContent =  brandHeader;


                    }
                    if (response['report'] == 'gallery') {
                        headerContent =  galleryHeader;


                    }

                    $('thead').removeClass('hidden');

                    $('[data-submit]').removeClass('hidden');


                        if (response['brands'].length > 0) {


                            if (!response['paginate']) {
                            
                                // table[key].rows().remove();

                                $('.portfolio-container').html('');
                            }

                            //remove default keys
                            $('.portfolio #portfolio-flters li').remove();

                            $.each(response['brands'], function(key, value) {
                           
                                $('.portfolio #portfolio-flters ul').append('<li data-filter=".filter-' + value.id + '">' + value.name + '</li>');
                                 /**customize portfolio-item**/
                                var portfolio_item = '<div class="col-lg-12 col-md-6 portfolio-item filter-' + value.id + '" data-filter-' + value.id + '>'

                                +'<table id="target" class="table table-striped" style="width:100%">' +
                                '<thead class="thead-dark hidden">' +
                                '<tr>' +
                                headerContent
                                +
                                '</tr>' +
                                '</thead>' +
                                '<tbody></tbody>' +
                                '</table>' +
                                '</div>';
                                /**ends**/

                 
                                //replace dynamic keys
                                $('.portfolio-container').append(portfolio_item);
                                // $('.row.portfolio-container.aos-init.aos-animate').css('height', '100% !important');

                            });
                            //make first dynamic tab active
                            $('#portfolio-flters li:nth-child(1)').addClass('filter-active');

                           
                        } else {

                            if (response['data'].length <= 0) {
                                //no brands
                                $('.portfolio-container').html('');

                                $('#portfolio-flters').find('li').not(':first').remove();
                            }

                        }

                }

                //for all other requests than brand
                $('thead').removeClass('hidden');

                    if (response['paginate'] == 1) {

                        // $('#target').DataTable().clear();
                    }

                   


                    $.each(response['data'], function(key, value) {

                        var table_rows = $.trim(value);

                        if (!table[key]) {
     

                        

                            if (response['paginate'] == 'undefined' && response['search'] > 0) {
                              

                             }
                            
                            table[key] = $('.filter-' + key + ' #' + settings.target).DataTable({
                                    'iDisplayLength': settings.rows,
                                    // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                    "bDestroy": true,
                                    "responsive": true,
                                    "scrollX": true,
                                    "ordering":false,
                                    dom: 'Bfrtip',
                                    "drawCallback": function( settings ) {
                                    $('.filter-' + key + ' #' + settings.target+' thead').remove(); } ,
                                    buttons: [
                                        {
                                            extend :'copy',
                                            exportOptions: {
                                                columns: ':visible:not(.not-exported)',
                                                modifier: { page: 'all' }
                                            },
                                            title: 'Copy'
                                        },
                                        {
                                            extend :'csv',
                                            exportOptions: {
                                                columns: ':visible:not(.not-exported)',
                                                modifier: { page: 'all' }
                                            },
                                            title: 'CSV export'
                                        },
                                        {
                                            extend : 'pdf' ,
                                            pageSize: 'LETTER',
                                            orientation: 'landscape',
                                            exportOptions: {
                                                columns: ':visible:not(.not-exported)',
                                                modifier: { page: 'all' },
                                            },
                                            title: 'Pdf export',
                                        },
                                         //,'pageLength'
                                    ]
                                });

                             xyz = table[key];
                             activeTab = table[key];
                
                             if(window.matchMedia("(max-width: 767px)").matches){
                                var totalColumns = xyz.columns(':visible').count();
                               
                                for(var i = 2; i< totalColumns; i++) {
                                   
                                        var column2 = xyz.column(i);
                                        column2.visible(!column2.visible());
                                        $('[data-column-toggle] [data-column="'+i+'"]').addClass('selectedToggle');
                                }
                             } else {

                                if (settings.column) {
           
                                 $.each(settings.column, function(key, value) {
                 
                                    var column2 = xyz.column(value);

                                    column2.visible(!column2.visible());
                                    $('[data-column-toggle] [data-column="'+value+'"]').addClass('selectedToggle');


                                });
                                }
                            }


                        } else {
                         //empty the default content
              

                            if (response['fresh'] && !response['paginate']) {
             
                                //     console.log('remove me');
                                    table[key].rows().remove();
                              

                            }
                        }


                            

                        // }
              
                    if(keyCount == 'undefined') {
                        keyCount = [key];
                        keyCount[key] = response['rowCountByKey'][key];

                    }

    
                    keyCount = client.sumByKey(key, $('#portfolio-flters li[data-filter=".filter-' + key + '"] [data-key-count]'), response['rowCountByKey'][key]);
                   
                   var windowHeight = $("body").height();
                   windowHeight = (windowHeight + 200);
                   console.log(windowHeight);
                   
     
                   $('.row.portfolio-container.aos-init.aos-animate').css('height', windowHeight);

                    table[key].rows.add($(table_rows)).draw();

                    });

    
               
                    $('[data-loading-spinner]').addClass('hidden');
                    //pagination
                    if (response['more']) {

                     $('body .filter-active').trigger('click');
                            
                            if (sessionStorage.getItem("stop_current_loop") == 1) {

                                console.log('stop now');
                            sessionStorage.setItem("more", 0);
                            sessionStorage.setItem("stop_current_loop", 0);

                            } else {
        
                            $('[data-loading-spinner]').removeClass('hidden');
                            settings.fresh = 0;
                            sessionStorage.setItem("more", 1);

                            rowCount = response['rowCountByKey'];
                            client.updateTable(response['offset'], 2, sessionStorage.getItem("stop_current_loop"), keyCount);
                            sessionStorage.setItem("stop_current_loop", 0);
                            sessionStorage.setItem("more", 0);


                            }

                        
            
                    } else {
                        // console.log('all done');
                      
                    }

                    settings.counter++;
       
                //adjust the layout
                if (settings.counter == 1) {
                    $('body .filter-active').trigger('click');

                }



           
            //toggle column display
                $('body').on('click', 'a[data-column]', function (e) {
                    e.preventDefault();

                    if (activeTab) {
                        var activeKey = $('li.filter-active').data('filter').split('filter-');

                        var i = $(this).attr('data-column');

                        //Get the column API object
                        var column = table[activeKey[1]].column(i);
             

                        
                        //Toggle the visibility
                        column.visible(!column.visible());
                        $('[data-column-toggle] [data-column="'+i+'"]').toggleClass('selectedToggle');

                    }

                });



            },

            sumByKey: function(key, element, total) {
                
                // var eThis = $(element).find('p');
                // var eVal = parseFloat((eThis.text() == '' ? 0 : eThis.text()));
                // var sum = eVal + total;

                // element.html('<p>'+sum+'<p>');


                // return test;
            

            },
            /**
             * Calls to ajax to fetch data
             * @param  {[type]} offset [start point to fetch data]
             * @return {[type]}        [response data]
             */
            updateTable: function(offset, $paginate, stopNow = false, rowCount) {

                 client.fireAjax(offset, $paginate, client[settings.target], rowCount);


            },

            updateCart: function($this) {

                checkboxid = $($this).attr('id');


                if ($($this).prop("checked")) {

                    //check if added into the cart already
                    if (jQuery.inArray(checkboxid, selectedListing) === -1) {


                        selected.push($($this).parents('[data-item-col]').find('[data-price]').val());

                        selectedListing.push(checkboxid);
                        $($this).prop("checked", true);
                        $($this).parents('[data-item-col]').css('background-color', 'rgb(60, 179, 113, 0.5)');

                        listingName = $('#' + checkboxid).parents('[data-item-col]').find('[data-title]').text();

                        selectedListingData.push([{
                            'id': checkboxid,
                            'listing': listingName,
                            'price': $('#' + checkboxid).parents('[data-item-col]').find('[data-price]').val()
                        }]);
                    }


                } else {
                    selected.splice($.inArray($($this).parents('[data-item-col]').find('[data-price]').val(), selected), 1);
                    selectedListing.splice($.inArray(checkboxid, selectedListing), 1);

                    $($this).prop("checked", false);
                    $($this).parents('[data-item-col]').css('background', 'none');


                }


                sum = 0;
                $.each(selected, function() { sum += parseFloat(this) || 0; });
                $('[data-price-total]').text(sum.toFixed(2));

                $('[data-qty-total]').text(selectedListing.length);

            },

            init: function() {

                $('[data-loading-spinner]').removeClass('hidden');
                client.updateTable(0);
            }

        };

        /**
         * initialize the modules in the plugin
         */
        // if (!jQuery.fn.datalist) {

            client.init();
        // }


        //update cart when listing checked
        $('body').on('change', 'input[type=checkbox]', function() {
            client.updateCart(this);

        });

        //solution to submit only selected checbox row,
        //remove name from row, and assign only after checked, use data attribute to store the name,
        //// then copy into name attribute
        //by default check all orders
        $('#ship_all').on('change', function() {
    
            var activeKey = $('li.filter-active').data('filter').split('filter-');
            
            if ($(this).is(":checked")) {
        

                $('.filter-'+activeKey[1]+' input[type=checkbox]:not(:disabled)').prop('checked', true);


            } else {

                $('.filter-'+activeKey[1]+' input[type=checkbox]').prop('checked', false);
            }

        });

        //make the first child active as 'All' is  removed
        $('#portfolio-flters li:nth-child(1)').addClass('filter-active');
        var activeKey = $('li.filter-active').data('filter').split('filter-');
        $('input[name=active_tab]').val(activeKey[1]);
        //hide all content
            $('.portfolio-item').css({ 'display': 'none' });
//show the first one
            $('.portfolio-item.filter-'+activeKey[1]).css({ 'display': 'block' });



        //enable content change according to key when keys chnaged dynamically
        $('body').on('click', '#portfolio-flters li', function() {


            var str = $(this).data('filter');
            var id = str.split(".filter-")[1];

            $('.portfolio-item').css({ 'display': 'none' });

            $('.filter-' + id).css({ 'display': 'block' });
            $(this).addClass('filter-active');
            
       

            //extra job

            // switch (id) { 
            //  case 'completed': 
            //      $('#correct_shipping_fee').removeClass('hidden');
            //         $('label[for="correct_shipping_fee"]').removeClass('hidden');
            //         $('#generate_ups').addClass('hidden');
            //         $('label[for="generate_ups"]').addClass('hidden');
            //      break;
            //  case 'under-process': 
            //      $('#generate_ups').removeClass('hidden');
            //         $('label[for="generate_ups"]').removeClass('hidden');
            //         $('#correct_shipping_fee').addClass('hidden');
            //         $('label[for="correct_shipping_fee"]').addClass('hidden');
            //      break;
            //  default:
            //      $('#generate_ups').addClass('hidden');
            //         $('label[for="generate_ups"]').addClass('hidden');
            //      $('#correct_shipping_fee').addClass('hidden');
            //         $('label[for="correct_shipping_fee"]').addClass('hidden');
            // }
            

            $('#portfolio-flters li:not(.filter-' + id + ')').removeClass('filter-active');
            $('#portfolio-flters li[data-filter=".filter-' + id + '"]').addClass('filter-active');

        });


        //replace data-name with name for selected checkboxes
        $('[data-form-submit]').one('click', function(e) {
            e.preventDefault();
            

             var activeKey = $('li.filter-active').data('filter').split('filter-');
             $('input[name=active_tab]').val(activeKey);

            $('.filter-'+activeKey[1]+' input[type=checkbox]').each(function(index, value) {
             
             if($(this).is(":checked")) {
               

                $(this).closest('tr').each(function(index, value) {

                   $(value).find('td').each(function(i, v) {
                    
                    //remove checkbox
                     $(v).find('.data-check-num').removeAttr('name');
                     
                    //select type
                     $(v).find('select').attr('name', $(v).find('select').attr('data-name'));
                     
                     $(v).find('select').removeAttr('data-name');
                     $(v).find('select').css('background', 'yellow');

                    //input type

                    $(v).find('input').attr('name', $(v).find('input').attr('data-name'));

                     $(v).find('input').removeAttr('data-name');
                    $(v).find('input').css('background', 'red');
                    
                   });
                    
                });
             }
            });
            if(activeKey=='pending') {
            sessionStorage.setItem("current_status", 'under-process');

            } else if(activeKey=='under-process') {
            sessionStorage.setItem("current_status", 'completed');

            } else {
            sessionStorage.setItem("current_status", '');

            }

            $('#form').submit();
        });

    };


}(jQuery));
