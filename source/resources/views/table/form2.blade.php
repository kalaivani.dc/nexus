<div class="card shadow-none border">
   
        <div class="card-header border-bottom">

            <div class="row"  style="width: 100%;">
                <div class="col-7">
                     @include('table.search')

                </div>
                <div class="col-5 text-right">
                    @include('table.action_buttons')

                </div>

                <div class="col-12">
                    @include('table.mobile_keys')
                    
                </div>
                <div class="col-12">
                    @include('table.desktop_keys')
                    
                </div>
                <div class="col-12">
                    @include('table.toggle_column')
                    
                </div>
                

            </div>

        </div>
 
    <form action="<?php echo (isset($save)?$save:'')?>" method="POST">
        @csrf
        <div class="card-body">
            <div class="col-12 text-center hidden" data-loading-spinner>
                <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            </div>



            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                <?php
                if (isset($keys)) {
                foreach ($keys as $b => $brand) {
                ?>
                <div class="col-lg-12 col-md-6 hidden portfolio-item filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>" data-filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>>

                @include('table.table2')


                </div>
                    <?php
                }
                }
                ?>
            </div>

            <?php 
            if (isset($save)) {
                ?>
                <button type="submit" class="btn btn-secondary btn-block subtotal-preview-btn" data-submit>{{__('product.submit')}}</button>
                <?php
            }
            ?>
            

        </div>
    </form>
</div>