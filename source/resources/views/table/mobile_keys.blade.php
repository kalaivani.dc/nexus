
<div id="portfolio-flters" class="btn-group d-sm-none hide-not-for-small">
    <div class="dropdown">
        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:100%;">
        Menu
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <ul  data-aos="fade-up" data-aos-delay="100">
            <?php
            if (isset($keys)) {


            foreach ($keys as $b => $brand) {
            if (is_array($brand)) {
             
                foreach ($brand as $key => $value) {
                   
                ?>
                 <li data-filter=".filter-<?php echo (isset($value['id'])? $value['id']: $value);?>" class="dropdown-item"><?php echo (isset($value['name'])? $value['name']: $value);?><div data-key-count></div></li>
                <?php
                }

            } else {
                 
                ?>
                <li data-filter=".filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>" class="dropdown-item"><?php echo (isset($brand->name)? $brand->name: $brand);?><div data-key-count></div></li>
                <?php

            }
            ?>

            
            <?php
            }
            }
            ?>
            </ul>
        </div>
    </div>


<!--<div class="col-lg-6 sm-12 text-right">-->
    <!--total-->
<!--    <button class="btn btn-outline-primary btn-group d-sm-none">-->
<!--        <div data-key-count style="top: -20px;"><span id="totalRows2"></span></div>-->
<!--         <div class="fonticon-wrap">-->
<!--          Total <i class="bx bx-heart" style="color: red;"></i>-->
<!--        </div>-->
<!--    </button>-->
   
<!--  </div>-->
</div>
