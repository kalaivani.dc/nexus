@extends('layouts.app')


@section('assets')
    {{Html::style('app-assets/vendors/css/extensions/swiper.min.css')}}
     {{Html::style('app-assets/css/pages/dashboard-ecommerce.css')}}

    {{Html::style('app-assets/vendors/css/editors/quill/katex.min.css')}}
    {{Html::style('app-assets/vendors/css/editors/quill/monokai-sublime.min.css')}}
    {{Html::style('app-assets/vendors/css/editors/quill/quill.snow.css')}}
    {{Html::style('app-assets/vendors/css/editors/quill/quill.bubble.css')}}
@endsection

@section('content')
    <div class="col-12">

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Edit Product</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/home"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/backend/<?php echo $user->role;?>/product">{{__('Product')}}</a></li>
                        <li class="breadcrumb-item active"><a href="/backend/<?php echo $user->role;?>/product?action=update&id=<?php echo $id;?>">Edit Product</a></li>
                    </ol>
                </div>
        </div>
    </div>
</div>
</div>


        
<form action="/backend/<?php echo $user->role;?>/product?action=update&id=<?php echo $id;?>" method="POST" enctype="multipart/form-data">@csrf
    <div class="col-12">
    <div class="card">
         <div class="card-header">
            <h4 class="card-title">Product</h4>
        </div>
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-3">
                <div class="form-group">
                <label>Product Name</label>
                <input type="text" name="name" class="form-control" value="<?php echo (!empty($product->name)? $product->name:'');?>">
                </div>
            </div>
             <!--<div class="col-md-3">-->
             <!--   <div class="form-group">-->
             <!--   <label>Brand</label>-->
             <!--   <select class="form-control" name="brand[]">-->
                <?php 
                // if(isset($brand)) {
                //     foreach ($brand as $key => $value) {
                       ?>
                      <!-- <option value="<?php #echo $value->id;?>"><?php #echo $value->name;?></option>-->
                       <?php
                //     }
                // }
                ?>
            <!--    </select>-->
            <!--</div>-->
            <!--</div>-->

             <div class="col-md-3">
                <div class="form-group">
                <label>Regular Price</label>
                <input type="text" class="form-control" name="regular_price" placeholder="Price" value="<?php echo (!empty($product->regular_price)? $product->regular_price:'');?>">
                </div>
             </div>

             <div class="col-md-3">
                <div class="form-group">
                <label>Sale Price</label>
                <input type="text" class="form-control" name="sale_price" placeholder="Price" value="<?php echo (!empty($product->sale_price)? $product->sale_price:'');?>">
                </div>
             </div>
 
              <div class="col-md-3">
                <div class="form-group">
                    <?php
                    $date = strtotime($product->start_date);
                    
                    ?>
                <label>Start Date</label>
                <input type="date" class="form-control" name="start_date" value="<?php echo ($product->start_date != '0000-00-00 00:00:00'? date('Y-m-d', $date):'');?>">
                </div>
             </div>
             
             <div class="col-md-3">
                <div class="form-group">
                     <?php
                    $date = strtotime($product->end_date);

                    ?>
                <label>End Date</label>
                <input type="date" class="form-control" name="end_date" value="<?php echo ($product->end_date != '0000-00-00 00:00:00'? date('Y-m-d', $date):'');?>">
                </div>
             </div>
             
            

             <div class="col-md-3">
                <div class="row">
                    <div class="col-md-8">
                         <label>Category</label>
                        <div class="form-label-group position-relative has-icon-left">
                            <input type="text" id="category" class="form-control" name="fname-floating-icon" placeholder="Category" data-category>
              
                             
                            <div class="form-control-position">
                                <i class="bx bx-user"></i>
                            </div>
                            <label for="category">Category</label>
                        </div>
                    </div>

                     <div class="col-md-4">
                         <label>Search</label>
                        <button type="button" id="categorySearch"  class="btn btn-icon btn-outline-primary mr-1 mb-1"><i class="bx bx-search"></i></button>
                    </div>
                </div>
                

                <div class="col-12">
                    <div id="categorySuggestion"></div>
                </div>
            </div>
            
            
          
        </div>

    </div>

        
</div>


</div>
     <div class="row col-12">
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Images</h4>
                        </div>
                        <div class="card-body">
                                
     <div class="row">
            <div class="col-md-12">
                <div class="form-label-group position-relative has-icon-left">
                    <input type="file" name="featured_pic"  id="email-id-floating-icon" class="form-control" name="email-id-floating-icon" placeholder="Featured Image">
                    
                    
                    <div class="form-control-position">
                        <i class='bx bx-photo-album'></i>
                    </div>
                    
                    <label for="email-id-floating-icon">Featured Image</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-label-group position-relative has-icon-left">
                    <input type="file" id="contact-floating-icon" class="form-control" name="gallery[]" placeholder="Gallery" multiple="multiple">
                    <div class="form-control-position">
                        <i class='bx bx-select-multiple'></i>
                    </div>
                    <label for="contact-floating-icon">Gallery</label>
                </div>
            </div>
        </div>
        
                             <div class="row">
            <div class="col-md-12">
                                
                           
                            <div class="swiper-responsive-breakpoints swiper-container">
                                <div class="swiper-wrapper">
                                    <?php 
                                    if (isset($images)) {
                                        foreach($images as $key=>$value) {
                                            ?>
                                        <div class="swiper-slide"><img class="img-fluid" src="/source/public/<?php echo $value;?>" alt="banner">
                                        <a href="/backend/admin/product?action=deleteImage&id=<?php echo $product->id;?>&src=<?php echo base64_encode($value);?>">
                                        <button type="button" class="btn btn-light-danger mr-1 mb-1" fdprocessedid="gu4wsc">Delete</button></a>
                                            </div>
                                            <?php
                                        }
                                    }
                                    
                                    ?>
                                    
                                    
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                            </div>
                             </div>
                              </div>
                        </div>
                    </div>
                 </div>
                 </div>
             
                
                   
                
    <!--existing variation-->
        <div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Existing Variation</h4>
        </div>
        <div class="card-body">
            <div class="col-12">
                <?php 
                $countM = 0;

                foreach ($variation as $key => $value) {
                  
                    $countM++;
                    ?>
                    
                    

            <div class="form-group table-responsive">
                <table class="table-stripped">

                    <tr id="<?php echo $countM;?>">
                        <th style="width: 10px;">

                        <span class="numberCircle" data-number><span class="badge badge-circle badge-secondary"><?php echo $countM;?></span></span>
                        </th>
                       
                        <th><input type="text" class="form-control" name="variation[<?php echo $countM;?>][variation]" value="<?php echo $key;?>"></th>
                         <th>
                            <button data-add-productspecs class="btn btn-icon btn-success" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg></button>
                                
                               <a href="/backend/admin/product?action=deleteVariation&id=<?php echo $key;?>">
                            <button data-delete-product class="btn btn-icon btn-danger" type="button"><i class="bx bx-trash-alt"></i></button></a>
                           


                        </th>
                        
                    </tr>

                <!--variation 2-->
                
                <tr>
                    <td colspan="8">
                        <table class="table-stripped" data-product_wrapper>
                             <tr>
                                

                                <td><code>Value</code></td>


                                <td><code>Regular Price</code></td>
                                <td><code>Promo Price</code></td>
                                <td><code>Promo Start Date</code></td>
                                <td><code>Promo End Date</code></td>
                                <td>
                                    <button data-add-productspecs class="btn btn-icon btn-success" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg></button>
                                </td>
                            </tr>
                        <?php 
                        if(isset($value)) {
                            
                       
                        $count = 0;
                        foreach ($value as $k => $v) {
                            $count++;
                        
                        ?>
                        
                           
                            <tr data-variant-wrapper>

                                <td>

                                <input type="text" name="variation[<?php echo $countM;?>][<?php echo $v->variant_id;?>][value]" value="<?php echo $v->value;?>" class="form-control">
                                </td>
                                <td>

                               
                                <input type="text" name="variation[<?php echo $countM;?>][<?php echo $v->variant_id;?>][price][regular]"  value="<?php echo $v->regular_price;?>" class="form-control">
                                </td>
                                <td>


                                <input type="text" name="variation[<?php echo $countM;?>][<?php echo $v->variant_id;?>][price][sale]"  value="<?php echo $v->sale_price;?>" class="form-control">
                                </td>

                                <td>

                                <input type="date" name="variation[<?php echo $countM;?>][<?php echo $v->variant_id;?>][sale][start_date]"  value="<?php echo $v->start_date;?>" class="form-control">
                                </td>
                                <td>

                                <input type="date" name="variation[<?php echo $countM;?>][<?php echo $v->variant_id;?>][sale][end_date]"  value="<?php echo $v->end_date;?>" class="form-control">
                                
                                </td>
                                 <td data-delete-variant>
                                      <a href="/backend/admin/product?action=deleteVariant&id=<?php echo $v->variant_id;?>">
                                <button class="btn btn-icon btn-danger" type="button"><i class="bx bx-trash-alt"></i></button></a>
                                </td>
                                
                            </tr>
                        
                         <?php } }?>
                         </table>
                    </td>

                </tr>
           


                </table>

            </div>
             <?php 
                }
            ?>
            </div>
           
            </div>
        </div>
    </div>
    <!--existing variation ends-->

    <!--variation-->
    <div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">New Variation</h4>
        </div>
        <div class="card-body">
            <div class="col-12">

            <div class="form-group table-responsive">
                <table class="table-stripped variation-wrapper" data-product_wrapper>

                    <tr id="<?php echo (isset($countM)? $countM+1: 1);?>">
                        <th style="width: 10px;">

                        <span class="numberCircle" data-number><span class="badge badge-circle badge-secondary"><?php echo (isset($countM)? $countM+1: 1);?></span></span>
                        </th>
                        
                        <th><input type="text" data-product-name class="form-control" name="new_variation[1][variation]" placeholder="Color"></th>

                        <th>

                       


                        </th>
                        <th>
                        <button data-add-product class="btn btn-icon btn-success" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg></button>
                        <button data-delete-product class="btn btn-icon btn-danger" type="button"><i class="bx bx-trash-alt"></i></button>
                    </th>
                    </tr>

                <!--variation 2-->
                <tr  class="variation_wrapper2">
                    <td colspan="8">
                        <table class="table-stripped">
                            <tr>
                                <th colspan="6"></th>
                                
                            </tr>

                            <tr><th>Attribute</th></tr>
                            <tr>
                               

                                <td><code>Value</code></td>


                                <td><code>Regular Price</code></td>
                                <td><code>Promo Price</code></td>
                                <td><code>Promo Start Date</code></td>
                                <td><code>Promo End Date</code></td>
                                 <td>
                                    <button data-add-productspecs2 class="btn btn-icon btn-success" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg></button>
                                </td>
                            </tr>
                            <tr data-variant-wrapper id="1">

                               
                                <td>

                                <input data-variation-value placeholder="yellow" type="text" name="new_variation[1][1][value]" class="form-control">
                                </td>
                                <td>

                               
                                <input data-regular-price placeholder="" type="text" name="new_variation[1][1][price][regular]" class="form-control">
                                </td>
                                <td>


                                <input data-sale-price placeholder="" type="text" name="new_variation[1][1][price][sale]" class="form-control">
                                </td>

                                <td>

                                <input data-sale-start type="date" name="new_variation[1][1][sale][start_date]" class="form-control">
                                </td>
                                <td>

                                <input data-sale-end type="date" name="new_variation[1][1][sale][end_date]" class="form-control">
                                
                                </td>
                                 <td data-delete-variant>
                                <button class="btn btn-icon btn-danger" type="button"><i class="bx bx-trash-alt"></i></button>
                                </td>

                            </tr>
                        </table>
                    </td>

                </tr>


                </table>

            </div>
            </div>
            </div>
        </div>
    </div>


    <!--description-->
       <div class="col-12">
        <div class="card">

         <div class="card-body">
             <textarea name="content" id="content" class="hidden"></textarea>
                       <section class="full-editor">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Description</h4>
                                </div>
                                <div class="card-body">
                                  
                                    <div class="row">
                                      <div class="col-sm-12">
                                            <div id="full-wrapper">
                                                <div id="full-container">
                                                    <div class="editor">
                                                        
                                                        <?php echo (!empty($product->content)? $product->content: '');?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                                </div>
                            </div>
                        </div>

                    

    <div class="mb-4 ml-1 d-flex text-right">

        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('submit')}}</button>
  </div>
</form>


@endsection
@section('scripts')

    {{Html::script('app-assets/vendors/js/extensions/swiper.min.js')}}
    {{Html::script('app-assets/js/scripts/extensions/swiper.js')}}
    

    {{Html::script('app-assets/vendors/js/editors/quill/katex.min.js')}}
    {{Html::script('app-assets/vendors/js/editors/quill/highlight.min.js')}}
    {{Html::script('app-assets/vendors/js/editors/quill/quill.min.js')}}
    {{Html::script('app-assets/js/scripts/editors/editor-quill.js')}}

  
<script type="text/javascript">

$(function() {
    
     //on page load
     var new_variation_start = $('[data-product_wrapper]').length;
     
        $('input[name="new_variation[1][variation]"]').attr('name', 'new_variation['+new_variation_start+'][variation]');
        
        $('input[name="new_variation[1][1][value]"]').attr('name', 'new_variation['+new_variation_start+'][1][value]');
        $('input[name="new_variation[1][1][price][regular]"]').attr('name', 'new_variation['+new_variation_start+'][1][price][regular]');
        $('input[name="new_variation[1][1][price][sale]"]').attr('name', 'new_variation['+new_variation_start+'][1][price][sale]');
        
        

       //variation
    $("body").on("click", "[data-add-product]", function() {
    
        var $trLast =  $($(this).parents('[data-product_wrapper]'));
        var id = $('[data-product_wrapper]').length;
        id++;

        //get total attribute to determine id
         var attrID = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;


        $trNew = $trLast.clone();
        $trNew.find('tr').attr('id', id);

        //variation id
        $($trNew).find('tr [data-number]').text(id).addClass('badge badge-circle badge-secondary');

        

        /**renumber product element**/
        $($trNew).find('tr th [data-product-name]').attr('name', 'variation['+id+'][variation]');


        //renumber copied attributes
        var rno = 0;
        $.each($($trNew).find('.variation_wrapper2 [data-variant-wrapper]'), function(key, value) {
           rno++;

           $(value).find('td input[data-variation-value]').attr('name', 'variation['+id+']['+rno+'][value]');
           $(value).find('td input[data-regular-price]').attr('name', 'variation['+id+']['+rno+'][price][regular]');
           $(value).find('td input[data-sale-price]').attr('name', 'variation['+id+']['+rno+'][price][sale]');
           $(value).find('td input[data-sale-start]').attr('name', 'variation['+id+']['+rno+'][sale][start_date]');
           $(value).find('td input[data-sale-end]').attr('name', 'variation['+id+']['+rno+'][sale][end_date]');
        });

        $($trNew).insertBefore($trLast);


  });
    //existing attributes
    $("body").on("click", "[data-add-productspecs]", function() {
        var id = $(this).parents('[data-product_wrapper]').find('tr').attr('id');

        var $trLast =  $($(this).parents('.variation_wrapper2')).find('table tr:last');
        $trNew = $trLast.clone();

         //get total attribute to determine id
        var rno = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;

        rno++;

        $.each($($trNew), function(key, value) {
           
            $(value).find('td input[data-variation-value]').attr('name', 'variation['+id+']['+rno+'][value]');
           $(value).find('td input[data-regular-price]').attr('name', 'variation['+id+']['+rno+'][price][regular]');
           $(value).find('td input[data-sale-price]').attr('name', 'variation['+id+']['+rno+'][price][sale]');
           $(value).find('td input[data-sale-start]').attr('name', 'variation['+id+']['+rno+'][sale][start_date]');
           $(value).find('td input[data-sale-end]').attr('name', 'variation['+id+']['+rno+'][sale][end_date]');


        });
        
        $trLast.after($trNew);
     });
     
    
     //new attributes
    $("body").on("click", "[data-add-productspecs2]", function() {
        var id = $(this).parents('[data-product_wrapper]').find('tr').attr('id');

        var $trLast =  $($(this).parents('.variation_wrapper2')).find('table tr:last');
        $trNew = $trLast.clone();

         //get total attribute to determine id
        var rno = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;

        rno++;

        $.each($($trNew), function(key, value) {
           
            $(value).find('td input[data-variation-value]').attr('name', 'new_variation['+id+']['+rno+'][value]');
           $(value).find('td input[data-regular-price]').attr('name', 'new_variation['+id+']['+rno+'][price][regular]');
           $(value).find('td input[data-sale-price]').attr('name', 'new_variation['+id+']['+rno+'][price][sale]');
           $(value).find('td input[data-sale-start]').attr('name', 'new_variation['+id+']['+rno+'][sale][start_date]');
           $(value).find('td input[data-sale-end]').attr('name', 'new_variation['+id+']['+rno+'][sale][end_date]');


        });
        
        $trLast.after($trNew);
     });


    //save product
    $('body').on('click', '[data-submit]', function(e) {
        console.log('here');
        e.preventDefault = false;
        var status = false;
        if (status == false) {

            var quill = new Quill ('.editor');
            var quillHtml = quill.root.innerHTML.trim();
          
            
            $('#content').val(quillHtml);
            e.preventDefault = false;
        }
       
      
    });

    $('#categorySearch').on('click', function(e) {
        var keyword = $('#category').val();
        console.log(keyword);
         $.ajax({
            url: "/backend/admin/category?action=list",
            method: "POST",
            data:{
                "_token": "{{ csrf_token() }}",
                'keyword':keyword,
                'dataType':'html'
            },
            success: function (response) {
                console.log(response);
                $('#categorySuggestion').html(response);

            }
        });

    });

    $("body").on('click', "[data-delete-product]", function() {
            
        $($(this).parents('[data-product_wrapper]')).remove();
   });

   $("body").on('click', "[data-delete-variant]", function() {

        $($(this).parents('[data-variant-wrapper]')).remove();
   });



});
</script>
@endsection