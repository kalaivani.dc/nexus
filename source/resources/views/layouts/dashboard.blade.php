<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Nexus Technology - DEMO</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/https://nexustech.online/asset/front/images/favicon.png">
    {{Html::style('front/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}

    {{Html::style('front/css/style.css')}}

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
 <!--page specific styles-->
    @yield('styles')
    <style type="text/css">

  div[data-key-count] {
    width: 30px;
    border-radius: 15px;
    position: relative;
    left: 20px;
    background-color: red;
    padding: 0;
    color: white;
    font-size: 12px;
    text-align: center;
    top: -30px;
}
          /*--------------------------------------------------------------
# Portfolio
--------------------------------------------------------------*/
#portfolio-flters {
  list-style: none;
  margin-bottom: 20px;
}

#portfolio-flters li {
 cursor: pointer;
    display: inline-block;
    margin: 10px 5px;
    font-size: 15px;
    font-weight: 500;
    line-height: 1;
    color: #444444;
    transition: all 0.3s;
    padding: 2px;
    border-radius: 50px;
    font-family: "Poppins", sans-serif;
    text-align: center;
    height: 30px;
    background-color: darkgrey;
}
#portfolio-flters ul {
    left: -5%;
    position: relative;
}
#portfolio-flters li:hover, #portfolio-flters li.filter-active {
  background: #47b2e4;
  color: #fff;
}

.portfolio .portfolio-item {
  margin-bottom: 30px;
}

.portfolio .portfolio-item .portfolio-img {
  overflow: hidden;
}

.portfolio .portfolio-item .portfolio-img img {
  transition: all 0.6s;
}

.portfolio .portfolio-item .portfolio-info {
  opacity: 0;
  position: absolute;
  left: 15px;
  bottom: 0;
  z-index: 3;
  right: 15px;
  transition: all 0.3s;
  background: rgba(55, 81, 126, 0.8);
  padding: 10px 15px;
}

.portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  color: #fff;
  font-weight: 600;
  color: #fff;
  margin-bottom: 0px;
}

.portfolio .portfolio-item .portfolio-info p {
  color: #f9fcfe;
  font-size: 14px;
  margin-bottom: 0;
}

.portfolio .portfolio-item .portfolio-info .preview-link, .portfolio .portfolio-item .portfolio-info .details-link {
  position: absolute;
  right: 40px;
  font-size: 24px;
  top: calc(50% - 18px);
  color: #fff;
  transition: 0.3s;
}

.portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover {
  color: #47b2e4;
}

.portfolio .portfolio-item .portfolio-info .details-link {
  right: 10px;
}

.portfolio .portfolio-item:hover .portfolio-img img {
  transform: scale(1.15);
}

.portfolio .portfolio-item:hover .portfolio-info {
  opacity: 1;
}

/*--------------------------------------------------------------
# Portfolio Details
--------------------------------------------------------------*/
.portfolio-details {
  padding-top: 40px;
}

.portfolio-details .portfolio-details-slider img {
  width: 100%;
}

.portfolio-details .portfolio-details-slider .swiper-pagination {
  margin-top: 20px;
  position: relative;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet {
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 1;
  border: 1px solid #47b2e4;
}

.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
  background-color: #47b2e4;
}

.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(55, 81, 126, 0.08);
}

.portfolio-details .portfolio-info h3 {
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #eee;
}

.portfolio-details .portfolio-info ul {
  list-style: none;
  padding: 0;
  font-size: 15px;
}

.portfolio-details .portfolio-info ul li + li {
  margin-top: 10px;
}

.portfolio-details .portfolio-description {
  padding-top: 30px;
}

.portfolio-details .portfolio-description h2 {
  font-size: 26px;
  font-weight: 700;
  margin-bottom: 20px;
}

.portfolio-details .portfolio-description p {
  padding: 0;
}

.hidden {
    display: none;
    
}
.strike {
    text-decoration: line-through;
    font-size:20px;
}
.front thead, .front .dt-buttons.btn-group.flex-wrap {
     display: none;
}
.fullheight {
height: 100% !important;
}

a.linkcolor {
color: #727E8C !important;
text-decoration: underline;
}
tr.red a.linkcolor {
color: #fff !important;
}


/**mobile only*/
@media only screen and (max-width: 600px){
table.dataTable tbody td {

    display: block;
}
}
</style>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
             <h1 class="logo-abbr text-light">
            <a href="/" class="brand-logo">
              <span>nexus</span>
                <!--<img class="logo-abbr" src="https://nexustech.online/asset/front/images/logo.png" alt="">-->
                <!--<img class="logo-compact" src="https://nexustech.online/asset/front/images/logo-text.png" alt="">-->
                <!--<img class="brand-title" src="https://nexustech.online/asset/front/images/logo-text.png" alt="">-->
            </a>
            </h1>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
        
       


        
        
        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">
                                Shop
                            </div>
                        </div>
                        <ul class="navbar-nav header-right">
                          
             <!--                <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link  ai-icon" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.75 15.8385V13.0463C22.7471 10.8855 21.9385 8.80353 20.4821 7.20735C19.0258 5.61116 17.0264 4.61555 14.875 4.41516V2.625C14.875 2.39294 14.7828 2.17038 14.6187 2.00628C14.4546 1.84219 14.2321 1.75 14 1.75C13.7679 1.75 13.5454 1.84219 13.3813 2.00628C13.2172 2.17038 13.125 2.39294 13.125 2.625V4.41534C10.9736 4.61572 8.97429 5.61131 7.51794 7.20746C6.06159 8.80361 5.25291 10.8855 5.25 13.0463V15.8383C4.26257 16.0412 3.37529 16.5784 2.73774 17.3593C2.10019 18.1401 1.75134 19.1169 1.75 20.125C1.75076 20.821 2.02757 21.4882 2.51969 21.9803C3.01181 22.4724 3.67904 22.7492 4.375 22.75H9.71346C9.91521 23.738 10.452 24.6259 11.2331 25.2636C12.0142 25.9013 12.9916 26.2497 14 26.2497C15.0084 26.2497 15.9858 25.9013 16.7669 25.2636C17.548 24.6259 18.0848 23.738 18.2865 22.75H23.625C24.321 22.7492 24.9882 22.4724 25.4803 21.9803C25.9724 21.4882 26.2492 20.821 26.25 20.125C26.2486 19.117 25.8998 18.1402 25.2622 17.3594C24.6247 16.5786 23.7374 16.0414 22.75 15.8385ZM7 13.0463C7.00232 11.2113 7.73226 9.45223 9.02974 8.15474C10.3272 6.85726 12.0863 6.12732 13.9212 6.125H14.0788C15.9137 6.12732 17.6728 6.85726 18.9703 8.15474C20.2677 9.45223 20.9977 11.2113 21 13.0463V15.75H7V13.0463ZM14 24.5C13.4589 24.4983 12.9316 24.3292 12.4905 24.0159C12.0493 23.7026 11.716 23.2604 11.5363 22.75H16.4637C16.284 23.2604 15.9507 23.7026 15.5095 24.0159C15.0684 24.3292 14.5411 24.4983 14 24.5ZM23.625 21H4.375C4.14298 20.9999 3.9205 20.9076 3.75644 20.7436C3.59237 20.5795 3.50014 20.357 3.5 20.125C3.50076 19.429 3.77757 18.7618 4.26969 18.2697C4.76181 17.7776 5.42904 17.5008 6.125 17.5H21.875C22.571 17.5008 23.2382 17.7776 23.7303 18.2697C24.2224 18.7618 24.4992 19.429 24.5 20.125C24.4999 20.357 24.4076 20.5795 24.2436 20.7436C24.0795 20.9076 23.857 20.9999 23.625 21Z" fill="#4C8147"/>
                                    </svg>
                                    <span class="badge light text-white bg-primary rounded-circle">12</span>
                                </a>
                                <div class="dropdown-menu rounded dropdown-menu-right">
                                    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
                                        <ul class="timeline">
                                            <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2">
                                                        <img alt="image" width="50" src="https://nexustech.online/asset/front/images/avatar/1.jpg">
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Dr sultads Send you Photo</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2 media-info">
                                                        KG
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Resport created successfully</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2 media-success">
                                                        <i class="fa fa-home"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Reminder : Treatment Time!</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                             <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2">
                                                        <img alt="image" width="50" src="https://nexustech.online/asset/front/images/avatar/1.jpg">
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Dr sultads Send you Photo</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2 media-danger">
                                                        KG
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Resport created successfully</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="timeline-panel">
                                                    <div class="media mr-2 media-primary">
                                                        <i class="fa fa-home"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6 class="mb-1">Reminder : Treatment Time!</h6>
                                                        <small class="d-block">29 July 2020 - 02:26 PM</small>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <a class="all-notification" href="javascript:void(0)">See all notifications <i class="ti-arrow-right"></i></a>
                                </div>
                            </li>
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link bell bell-link" href="javascript:void(0)">
                                    <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.4604 3.84863H5.31685C4.64745 3.84937 4.00568 4.11561 3.53234 4.58895C3.059 5.06229 2.79276 5.70406 2.79202 6.37346V18.156C2.79276 18.8254 3.059 19.4672 3.53234 19.9405C4.00568 20.4138 4.64745 20.6801 5.31685 20.6808C5.54002 20.6809 5.75401 20.7697 5.91181 20.9275C6.06961 21.0853 6.15832 21.2993 6.15846 21.5224V23.3166C6.15846 23.6212 6.24115 23.9202 6.39771 24.1815C6.55427 24.4429 6.77882 24.6569 7.04744 24.8006C7.31605 24.9444 7.61864 25.0125 7.92295 24.9978C8.22726 24.9831 8.52186 24.8861 8.77536 24.7171L14.6173 20.8222C14.7554 20.7297 14.9179 20.6805 15.0841 20.6808H19.187C19.7383 20.6798 20.2743 20.4991 20.7137 20.1662C21.1531 19.8332 21.472 19.3661 21.6222 18.8357L24.8965 7.04986C24.9999 6.67457 25.0152 6.2805 24.9413 5.89831C24.8675 5.51613 24.7064 5.15615 24.4707 4.84638C24.235 4.53662 23.9309 4.28544 23.5823 4.11238C23.2336 3.93933 22.8497 3.84907 22.4604 3.84863ZM23.2733 6.6028L20.0006 18.3845C19.95 18.5612 19.8432 18.7166 19.6964 18.8272C19.5496 18.9378 19.3708 18.9976 19.187 18.9976H15.0841C14.5856 18.997 14.0981 19.1446 13.6836 19.4217L7.84168 23.3166V21.5224C7.84094 20.853 7.5747 20.2113 7.10136 19.7379C6.62802 19.2646 5.98625 18.9983 5.31685 18.9976C5.09368 18.9975 4.87969 18.9088 4.72189 18.7509C4.56409 18.5931 4.47537 18.3792 4.47524 18.156V6.37346C4.47537 6.15029 4.56409 5.9363 4.72189 5.7785C4.87969 5.6207 5.09368 5.53198 5.31685 5.53185H22.4604C22.5905 5.53218 22.7188 5.56252 22.8352 5.62052C22.9517 5.67851 23.0532 5.76259 23.1318 5.86621C23.2105 5.96984 23.2641 6.09021 23.2887 6.21797C23.3132 6.34572 23.3079 6.47742 23.2733 6.6028Z" fill="#4C8147"/>
                                        <path d="M7.84167 11.423H12.0497C12.2729 11.423 12.487 11.3343 12.6448 11.1765C12.8027 11.0186 12.8913 10.8046 12.8913 10.5814C12.8913 10.3581 12.8027 10.1441 12.6448 9.98625C12.487 9.82842 12.2729 9.73975 12.0497 9.73975H7.84167C7.61846 9.73975 7.4044 9.82842 7.24656 9.98625C7.08873 10.1441 7.00006 10.3581 7.00006 10.5814C7.00006 10.8046 7.08873 11.0186 7.24656 11.1765C7.4044 11.3343 7.61846 11.423 7.84167 11.423Z" fill="#4C8147"/>
                                        <path d="M15.4162 13.1064H7.84167C7.61846 13.1064 7.4044 13.1951 7.24656 13.3529C7.08873 13.5108 7.00006 13.7248 7.00006 13.9481C7.00006 14.1713 7.08873 14.3853 7.24656 14.5432C7.4044 14.701 7.61846 14.7897 7.84167 14.7897H15.4162C15.6394 14.7897 15.8534 14.701 16.0113 14.5432C16.1691 14.3853 16.2578 14.1713 16.2578 13.9481C16.2578 13.7248 16.1691 13.5108 16.0113 13.3529C15.8534 13.1951 15.6394 13.1064 15.4162 13.1064Z" fill="#4C8147"/>
                                    </svg>
                                    <span class="badge light text-white bg-primary rounded-circle">5</span>
                                </a>
                            </li> -->
                            <li class="nav-item dropdown notification_dropdown">
                                <a class="nav-link" href="javascript:void(0)" data-toggle="dropdown">
                                   <i class="fa fa-shopping-basket" style="color:rgb(76, 129, 71);"></i>
                                    <span class="badge light text-white bg-primary rounded-circle hidden" data-cart-wrapper>1</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right rounded">
                                    <div id="DZ_W_TimeLine11Home" class="widget-timeline dz-scroll style-1 p-3 height370 ps ps--active-y">
                                        <form action="<?php echo (isset($save)?$save:'')?>" method="POST">
                                            @csrf
                                            <ul class="contacts" data-cart>
                                        
                                            </ul>
                                            <div class="shopping-cart mt-3">
                                           
                                                     <button type="submit" class="btn btn-success subtotal-preview-btn" data-submit><i class="fa fa-shopping-basket mr-2"></i>Checkout</button>
                                              
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                   
                                    <div class="header-info">
                                        <ul class="navbar-nav ms-auto">
                                        <!-- Authentication Links -->
                                        @guest
                                            @if (Route::has('login'))
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                </li>
                                            @endif

                                            @if (Route::has('register'))
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li class="nav-item dropdown">
                                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    {{ Auth::user()->name }}
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        @endguest
                                    </ul>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="./app-profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item ai-icon">
                                        <svg id="icon-inbox" xmlns="http://www.w3.org/2000/svg" class="text-success" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                        <span class="ml-2">Inbox </span>
                                    </a>
                                    <a href="./page-login.html" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
                <ul class="metismenu" id="menu">
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-networking"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="index.html">Dashboard</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="order-id.html">Order ID</a></li>
                            <li><a href="general-customers.html">General Customers</a></li>
                            <li><a href="analytics.html">Analytics</a></li>
                            <li><a href="reviews.html">Reviews</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-television"></i>
                            <span class="nav-text">Apps</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./app-profile.html">Profile</a></li>
                            <li><a href="./post-details.html">Post Details</a></li>
                            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Email</a>
                                <ul aria-expanded="false">
                                    <li><a href="./email-compose.html">Compose</a></li>
                                    <li><a href="./email-inbox.html">Inbox</a></li>
                                    <li><a href="./email-read.html">Read</a></li>
                                </ul>
                            </li>
                            <li><a href="./app-calender.html">Calendar</a></li>
                            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Shop</a>
                                <ul aria-expanded="false">
                                    <li><a href="./ecom-product-grid.html">Product Grid</a></li>
                                    <li><a href="./ecom-product-list.html">Product List</a></li>
                                    <li><a href="./ecom-product-detail.html">Product Details</a></li>
                                    <li><a href="./ecom-product-order.html">Order</a></li>
                                    <li><a href="./ecom-checkout.html">Checkout</a></li>
                                    <li><a href="./ecom-invoice.html">Invoice</a></li>
                                    <li><a href="./ecom-customers.html">Customers</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-controls-3"></i>
                            <span class="nav-text">Charts</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./chart-flot.html">Flot</a></li>
                            <li><a href="./chart-morris.html">Morris</a></li>
                            <li><a href="./chart-chartjs.html">Chartjs</a></li>
                            <li><a href="./chart-chartist.html">Chartist</a></li>
                            <li><a href="./chart-sparkline.html">Sparkline</a></li>
                            <li><a href="./chart-peity.html">Peity</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-internet"></i>
                            <span class="nav-text">Bootstrap</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./ui-accordion.html">Accordion</a></li>
                            <li><a href="./ui-alert.html">Alert</a></li>
                            <li><a href="./ui-badge.html">Badge</a></li>
                            <li><a href="./ui-button.html">Button</a></li>
                            <li><a href="./ui-modal.html">Modal</a></li>
                            <li><a href="./ui-button-group.html">Button Group</a></li>
                            <li><a href="./ui-list-group.html">List Group</a></li>
                            <li><a href="./ui-media-object.html">Media Object</a></li>
                            <li><a href="./ui-card.html">Cards</a></li>
                            <li><a href="./ui-carousel.html">Carousel</a></li>
                            <li><a href="./ui-dropdown.html">Dropdown</a></li>
                            <li><a href="./ui-popover.html">Popover</a></li>
                            <li><a href="./ui-progressbar.html">Progressbar</a></li>
                            <li><a href="./ui-tab.html">Tab</a></li>
                            <li><a href="./ui-typography.html">Typography</a></li>
                            <li><a href="./ui-pagination.html">Pagination</a></li>
                            <li><a href="./ui-grid.html">Grid</a></li>

                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-heart"></i>
                            <span class="nav-text">Plugins</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./uc-select2.html">Select 2</a></li>
                            <li><a href="./uc-nestable.html">Nestedable</a></li>
                            <li><a href="./uc-noui-slider.html">Noui Slider</a></li>
                            <li><a href="./uc-sweetalert.html">Sweet Alert</a></li>
                            <li><a href="./uc-toastr.html">Toastr</a></li>
                            <li><a href="./map-jqvmap.html">Jqv Map</a></li>
                            <li><a href="./uc-lightgallery.html">Lightgallery</a></li>
                        </ul>
                    </li>
                    <li><a href="widget-basic.html" class="ai-icon" aria-expanded="false">
                            <i class="flaticon-381-settings-2"></i>
                            <span class="nav-text">Widget</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-notepad"></i>
                            <span class="nav-text">Forms</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./form-element.html">Form Elements</a></li>
                            <li><a href="./form-wizard.html">Wizard</a></li>
                            <li><a href="./form-editor-summernote.html">Summernote</a></li>
                            <li><a href="form-pickers.html">Pickers</a></li>
                            <li><a href="form-validation-jquery.html">Jquery Validate</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-network"></i>
                            <span class="nav-text">Table</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="table-bootstrap-basic.html">Bootstrap</a></li>
                            <li><a href="table-datatable-basic.html">Datatable</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-layer-1"></i>
                            <span class="nav-text">Pages</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./page-register.html">Register</a></li>
                            <li><a href="./page-login.html">Login</a></li>
                            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Error</a>
                                <ul aria-expanded="false">
                                    <li><a href="./page-error-400.html">Error 400</a></li>
                                    <li><a href="./page-error-403.html">Error 403</a></li>
                                    <li><a href="./page-error-404.html">Error 404</a></li>
                                    <li><a href="./page-error-500.html">Error 500</a></li>
                                    <li><a href="./page-error-503.html">Error 503</a></li>
                                </ul>
                            </li>
                            <li><a href="./page-lock-screen.html">Lock Screen</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="add-menu-sidebar">
                    <img src="https://nexustech.online/asset/front/images/food-serving.png" alt="">
                    <p class="mb-3">Organize your menus through button bellow</p>
                    <span class="fs-12 d-block mb-3">Lorem ipsum dolor sit amet</span>
                    <a href="javascript:void(0)" class="btn btn-secondary btn-rounded" data-toggle="modal" data-target="#addOrderModalside" >+Add Menus</a>
                </div>
                <div class="copyright">
                    <p>� 2023 All Rights Reserved</p>
                    <p>Made with <span class="heart"></span> by Nexus Tech</p>
                </div>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <!-- Add Order -->
                <div class="modal fade" id="addOrderModalside">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Menus</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Order Date</label>
                                        <input type="date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-black font-w500">Food Price</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-titles">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Layout</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Blank</a></li>
                    </ol>
                </div>
            
                <div id="app">
                   

                    <main class="py-12">
                       
                            <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                    <div class="row">
                        <!-- Website Analytics Starts-->
                        <div class="col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h4 class="card-title">Website Analytics</h4>
                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                </div>
                                <div class="card-body pb-1">
                                    <div class="d-flex justify-content-around align-items-center flex-wrap">
                                        <div class="user-analytics mr-2">
                                            <i class="bx bx-user mr-25 align-middle"></i>
                                            <span class="align-middle text-muted">Users</span>
                                            <div class="d-flex">
                                                <div id="radial-success-chart"></div>
                                                <h3 class="mt-1 ml-50">61K</h3>
                                            </div>
                                        </div>
                                        <div class="sessions-analytics mr-2">
                                            <i class="bx bx-trending-up align-middle mr-25"></i>
                                            <span class="align-middle text-muted">Sessions</span>
                                            <div class="d-flex">
                                                <div id="radial-warning-chart"></div>
                                                <h3 class="mt-1 ml-50">92K</h3>
                                            </div>
                                        </div>
                                        <div class="bounce-rate-analytics">
                                            <i class="bx bx-pie-chart-alt align-middle mr-25"></i>
                                            <span class="align-middle text-muted">Bounce Rate</span>
                                            <div class="d-flex">
                                                <div id="radial-danger-chart"></div>
                                                <h3 class="mt-1 ml-50">72.6%</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="analytics-bar-chart" class="my-75"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12 dashboard-referral-impression">
                            <div class="row">
                                <!-- Referral Chart Starts-->
                                <div class="col-xl-12 col-12">
                                    <div class="card">
                                        <div class="card-body text-center pb-0">
                                            <h2>$32,690</h2>
                                            <span class="text-muted">Referral 40%</span>
                                            <div id="success-line-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Impression Radial Chart Starts-->
                                <div class="col-xl-12 col-12">
                                    <div class="card">
                                        <div class="card-body donut-chart-wrapper">
                                            <div id="donut-chart" class="d-flex justify-content-center"></div>
                                            <ul class="list-inline d-flex justify-content-around mb-0">
                                                <li> <span class="bullet bullet-xs bullet-primary mr-50"></span>Social</li>
                                                <li> <span class="bullet bullet-xs bullet-info mr-50"></span>Email</li>
                                                <li> <span class="bullet bullet-xs bullet-warning mr-50"></span>Search</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-12 col-sm-12">
                            <div class="row">
                                <!-- Conversion Chart Starts-->
                                <div class="col-xl-12 col-md-6 col-12">
                                    <div class="card">
                                        <div class="card-header d-flex justify-content-between pb-xl-0 pt-xl-1">
                                            <div class="conversion-title">
                                                <h4 class="card-title">Conversion</h4>
                                                <p>60%
                                                    <i class="bx bx-trending-up text-success font-size-small align-middle mr-25"></i>
                                                </p>
                                            </div>
                                            <div class="conversion-rate">
                                                <h2>89k</h2>
                                            </div>
                                        </div>
                                        <div class="card-body text-center">
                                            <div id="bar-negative-chart" class="negative-bar-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar bg-rgba-primary m-0 p-25 mr-75 mr-xl-2">
                                                            <div class="avatar-content">
                                                                <i class="bx bx-user text-primary font-medium-2"></i>
                                                            </div>
                                                        </div>
                                                        <div class="total-amount">
                                                            <h5 class="mb-0">$38,566</h5>
                                                            <small class="text-muted">Conversion</small>
                                                        </div>
                                                    </div>
                                                    <div id="primary-line-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar bg-rgba-warning m-0 p-25 mr-75 mr-xl-2">
                                                            <div class="avatar-content">
                                                                <i class="bx bx-dollar text-warning font-medium-2"></i>
                                                            </div>
                                                        </div>
                                                        <div class="total-amount">
                                                            <h5 class="mb-0">$53,659</h5>
                                                            <small class="text-muted">Income</small>
                                                        </div>
                                                    </div>
                                                    <div id="warning-line-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Activity Card Starts-->
                        <div class="col-xl-3 col-md-6 col-12 activity-card">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Activity</h4>
                                </div>
                                <div class="card-body pt-1">
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-primary m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-bar-chart-alt-2 text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total Sales</small>
                                            <small class="float-right">$8,125</small>
                                            <div class="progress progress-bar-primary progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="50" style="width:50%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-success m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-dollar text-success"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Income Amount</small>
                                            <small class="float-right">$18,963</small>
                                            <div class="progress progress-bar-success progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="80" style="width:80%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-warning m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-stats text-warning"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total Budget</small>
                                            <small class="float-right">$14,150</small>
                                            <div class="progress progress-bar-warning progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" style="width:60%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex mb-75">
                                        <div class="avatar bg-rgba-danger m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-check text-danger"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Completed Tasks</small>
                                            <small class="float-right">106</small>
                                            <div class="progress progress-bar-danger progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="30" style="width:30%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Profit Report Card Starts-->
                        <div class="col-xl-3 col-md-6 col-12 profit-report-card">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <div class="card">
                                        <div class="card-header d-flex justify-content-between align-items-center">
                                            <h4 class="card-title">Profit Report</h4>
                                            <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                        </div>
                                        <div class="card-body d-flex justify-content-around">
                                            <div class="d-inline-flex mr-xl-2">
                                                <div id="profit-primary-chart"></div>
                                                <div class="profit-content ml-50 mt-50">
                                                    <h5 class="mb-0">$12k</h5>
                                                    <small class="text-muted">2020</small>
                                                </div>
                                            </div>
                                            <div class="d-inline-flex">
                                                <div id="profit-info-chart"></div>
                                                <div class="profit-content ml-50 mt-50">
                                                    <h5 class="mb-0">$64k</h5>
                                                    <small class="text-muted">2019</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Registrations</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="d-flex align-items-end justify-content-around">
                                                <div class="registration-content mr-xl-1">
                                                    <h4 class="mb-0">56.3k</h4>
                                                    <i class="bx bx-trending-up success align-middle"></i>
                                                    <span class="text-success">12.8%</span>
                                                </div>
                                                <div id="registration-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sales Chart Starts-->
                        <div class="col-xl-3 col-md-6 col-12 sales-card">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <div class="card-title-content">
                                        <h4 class="card-title">Sales</h4>
                                        <small class="text-muted">Calculated in last 7 days</small>
                                    </div>
                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                </div>
                                <div class="card-body">
                                    <div id="sales-chart" class="mb-2"></div>
                                    <div class="d-flex justify-content-between my-1">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-up-arrow-circle text-primary font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Best Selling</h6>
                                                <small class="text-muted">Sunday</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">28.6k</h6>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-down-arrow-circle icon-light font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Lowest Selling</h6>
                                                <small class="text-muted">Thursday</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">986k</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Growth Chart Starts-->
                        <div class="col-xl-3 col-md-6 col-12 growth-card">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButtonSec" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            2020
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonSec">
                                            <a class="dropdown-item" href="javascript:void(0);">2020</a>
                                            <a class="dropdown-item" href="javascript:void(0);">2019</a>
                                            <a class="dropdown-item" href="javascript:void(0);">2018</a>
                                        </div>
                                    </div>
                                    <div id="growth-Chart"></div>
                                    <h6 class="mt-2"> 62% Growth in 2020</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Task Card Starts -->
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card widget-todo">
                                        <div class="card-header border-bottom d-flex justify-content-between align-items-center flex-wrap">
                                            <h4 class="card-title d-flex mb-25 mb-sm-0">
                                                <i class='bx bx-check font-medium-5 pl-25 pr-75'></i>Tasks
                                            </h4>
                                            <ul class="list-inline d-flex mb-25 mb-sm-0">
                                                <li class="d-flex align-items-center">
                                                    <i class='bx bx-filter font-medium-3 mr-50'></i>
                                                    <div class="dropdown">
                                                        <div class="dropdown-toggle mr-1 cursor-pointer" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter</div>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="javascript:void(0);">My Tasks</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due this week</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due next week</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Custom Filter</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="d-flex align-items-center">
                                                    <i class='bx bx-sort mr-50 font-medium-3'></i>
                                                    <div class="dropdown">
                                                        <div class="dropdown-toggle cursor-pointer" role="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort</div>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                                                            <a class="dropdown-item" href="javascript:void(0);">None</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Alphabetical</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Due Date</a>
                                                            <a class="dropdown-item" href="javascript:void(0);">Assignee</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="card-body px-0 py-1">
                                            <ul class="widget-todo-list-wrapper" id="widget-todo-list">
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox1">
                                                                <label for="checkbox1"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add SCSS and JS files if required</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-success mr-1">frontend</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">RA</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox2">
                                                                <label for="checkbox2"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Check your changes, before commiting</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-danger mr-1">backend</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/social-2.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item completed">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox3" checked>
                                                                <label for="checkbox3"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Dribble, Behance, UpLabs & Pinterest Post</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-primary mr-1">UI/UX</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">JP</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox4">
                                                                <label for="checkbox4"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Fresh Design Web & Responsive Miracle</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-info mr-1">Design</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/user-05.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox5">
                                                                <label for="checkbox5"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add Calendar page, source and credit page in
                                                                documentation</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-warning mr-1">Javascript</div>
                                                            <div class="avatar bg-rgba-primary m-0 mr-50">
                                                                <div class="avatar-content">
                                                                    <span class="font-size-base text-primary">AK</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="widget-todo-item">
                                                    <div class="widget-todo-title-wrapper d-flex justify-content-between align-items-center mb-50">
                                                        <div class="widget-todo-title-area d-flex align-items-center">
                                                            <i class='bx bx-grid-vertical mr-25 font-medium-4 cursor-move'></i>
                                                            <div class="checkbox checkbox-shadow">
                                                                <input type="checkbox" class="checkbox__input" id="checkbox6">
                                                                <label for="checkbox6"></label>
                                                            </div>
                                                            <span class="widget-todo-title ml-50">Add Angular Starter-kit</span>
                                                        </div>
                                                        <div class="widget-todo-item-action d-flex align-items-center">
                                                            <div class="badge badge-pill badge-light-primary mr-1">UI/UX</div>
                                                            <div class="avatar m-0 mr-50">
                                                                <img src="../../../app-assets/images/profile/user-uploads/user-05.jpg" alt="img placeholder" height="32" width="32">
                                                            </div>
                                                            <div class="dropdown">
                                                                <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer icon-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a class="dropdown-item" href="javascript:void(0);">View Details</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Duplicate Task</a>
                                                                    <a class="dropdown-item" href="javascript:void(0);">Delete Task</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Daily Financials Card Starts -->
                        <div class="col-lg-5">
                            <div class="card ">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        Order Timeline
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <ul class="timeline mb-0">
                                        <li class="timeline-item timeline-icon-primary active">
                                            <div class="timeline-time">September, 16</div>
                                            <h6 class="timeline-title">1983, orders, $4220</h6>
                                            <p class="timeline-text">2 hours ago</p>
                                            <div class="timeline-content">
                                                <img src="../../../app-assets/images/icon/pdf.png" alt="document" height="23" width="19" class="mr-50">New Order.pdf
                                            </div>
                                        </li>
                                        <li class="timeline-item timeline-icon-primary active">
                                            <div class="timeline-time">September, 17</div>
                                            <h6 class="timeline-title">12 Invoices have been paid</h6>
                                            <p class="timeline-text">25 minutes ago</p>
                                            <div class="timeline-content">
                                                <img src="../../../app-assets/images/icon/pdf.png" alt="document" height="23" width="19" class="mr-50">Invoices.pdf
                                            </div>
                                        </li>
                                        <li class="timeline-item timeline-icon-primary active pb-0">
                                            <div class="timeline-time">September, 18</div>
                                            <h6 class="timeline-title">Order #37745 from September</h6>
                                            <p class="timeline-text">4 minutes ago</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Dashboard Analytics end -->
                   
                         
                    </main>

                </div>
    
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright � Designed &amp; Developed by <a href="http://nexustech.online/" target="_blank">Nexus Tech</a> 2023</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->

    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>


    <!-- Required vendors -->
    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}
    {{Html::script('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}
    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}
    {{Html::script('app-assets/js/core/app-menu.js')}}
    {{Html::script('app-assets/js/core/app.js')}}
    {{Html::script('app-assets/js/scripts/components.js')}}
    
    {{Html::script('app-assets/vendors/js/charts//apexcharts.min.js')}}
    {{Html::script('app-assets/vendors/js/extensions/dragula.min.')}}


    {{Html::script('assets/theme/vendor/aos/aos.js')}}
    {{Html::script('assets/theme/vendor/glightbox/js/glightbox.min.js')}}
    {{Html::script('assets/theme/vendor/isotope-layout/isotope.pkgd.min.js')}}
    {{Html::script('assets/theme/vendor/swiper/swiper-bundle.min.js')}}
    {{Html::script('assets/theme/vendor/waypoints/noframework.waypoints.js')}} 
    {{Html::script('assets/theme/js/main.js')}}

    {{Html::script('app-assets/js/scripts/footer.js')}}
    {{Html::script('js/app.js')}} 


      <!--do not change this-->
      <script src="{{ 'https://nexustech.online/asset/front/vendor/global/global_modified.min.js'}}" defer></script>
      <!-- <script src="{{ 'https://nexustech.online/asset/front/vendor/bootstrap-select/dist/js/bootstrap-select.min.js'}}" defer></script> -->
      <script src="{{ 'https://nexustech.online/asset/front/js/custom.min.js'}}" defer></script>
      <script src="{{ 'https://nexustech.online/asset/front/js/deznav-init.js'}}" defer></script>

@yield('scripts')




</body>

</html>