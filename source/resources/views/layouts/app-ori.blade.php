<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Nexus') }}</title>

    <!-- {{Html::style('app-assets/vendors/css/vendors.min.css')}} -->
    {{Html::style('app-assets/css/bootstrap.css')}}
    {{Html::style('app-assets/css/bootstrap-extended.css')}}
    {{Html::style('app-assets/css/colors.css')}}
    {{Html::style('app-assets/css/components.css')}}
    {{Html::style('app-assets/css/themes/dark-layout.css')}}
    {{Html::style('app-assets/css/themes/semi-dark-layout.css')}}

    {{Html::style('app-assets/css/core/menu/menu-types/vertical-menu.css')}}
    {{Html::style('app-assets/css/pages/dashboard-ecommerce.css')}}

    {{Html::style('assets/css/style.css')}}
    {{Html::style('assets/css/custom.css')}}

    <!--page specific styles-->
    @yield('styles')

</head>
<body>
    <div id="app">
        <header>
               <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Nexus') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
         </header>
         <aside>
             @include('layouts.side-menu')
         </aside>
      
        <div class="app-content content">
          <div class="content-overlay"></div>
          <div class="content-wrapper">
              <div class="content-header row"></div>
              <div class="content-body">

                <main class="py-4">
                    <div class="container">
                        @yield('content')
                    </div>
                     
                </main>
             </div>
          </div>
      </div>

    </div>
    <script type="text/javascript">
      var csrfToken = "{{ csrf_token() }}";

    </script>

    {{Html::script('app-assets/vendors/js/vendors.min.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}
    {{Html::script('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}
    {{Html::script('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}
    {{Html::script('app-assets/js/scripts/configs/vertical-menu-dark.js')}}
    {{Html::script('app-assets/js/core/app-menu.js')}}
    {{Html::script('app-assets/js/core/app.js')}}
    {{Html::script('app-assets/js/scripts/components.js')}}

    {{Html::script('assets/theme/vendor/aos/aos.js')}}
    {{Html::script('assets/theme/vendor/glightbox/js/glightbox.min.js')}}
    {{Html::script('assets/theme/vendor/isotope-layout/isotope.pkgd.min.js')}}
    {{Html::script('assets/theme/vendor/swiper/swiper-bundle.min.js')}}
    {{Html::script('assets/theme/vendor/waypoints/noframework.waypoints.js')}}
    {{Html::script('assets/theme/js/main.js')}}

    {{Html::script('app-assets/js/scripts/footer.js')}}
    {{Html::script('js/app.js')}}

    <!--page specific scripts-->
    @yield('scripts')



</body>
</html>
