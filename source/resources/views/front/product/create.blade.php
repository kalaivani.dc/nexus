@extends('layouts.app')


@section('assets')

@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Add Product</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/module/main"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/module/main">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
    
    </div>
<form action="<?php echo $site;?><?php echo $user->role;?>/shop/product?action=save" method="POST" enctype="multipart/form-data">@csrf
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-4">
                <div class="form-group">
                <label>{{__('module.name')}}</label>
                <input type="text" name="name" class="form-control">
                </div>
            </div>
             <div class="col-md-4">
                <select name="brand[]">
                <?php 
                if(isset($brand)) {
                    foreach ($brand as $key => $value) {
                       ?>
                       <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                       <?php
                    }
                }
                ?>
                </select>
            </div>

             <div class="col-md-4">
                <input type="text" name="regular_price" placeholder="Price">
             </div>
            
          
        </div>

         <!--for desktop-->
                                                    <div class="form-group table-responsive">
                                                        <table class="table-stripped">
                                                           
                                                            <!--variation 2-->
                                                           <tr class="variation_wrapper2">
                                                                <td colspan="8">
                                                                    <table class="table-stripped">
                                                                        <tr>
                                                                            <th colspan="5"></th>
                                                                            <th><input type="button" data-add-productspecs2 value="Add More Variations" class="btn btn-primary mr-1"></th>
                                                                        </tr>
                                                                         <tr>
                                                                <th><span>Variation Name</span></th>
                                                                <th><span>Variation Value</span></th>
                                                                <th><span>Regular Price</span></th>
                                                                <th><span>Promo Price</span></th>
                                                                <th><span>Promo Start Date</span></th>
                                                                <th><span>Promo End Date</span></th>
                                                 

                                                            </tr>
                                                                        <tr>
                                                                            
                                                              
                                                                 <td>
                                                                    
                                                                    <input placeholder="example: color" type="text" name="variation[1][name]" class="form-control">
                                                                </td>
                                                                <td>
                                                                   
                                                                    <input  placeholder="example: yellow" type="text" name="variation[1][value]" class="form-control">
                                                                </td>
                                                                 
                                                                <td>
                                                                    
                                                                    <input type="hidden" name="currency" value="" class="form-control">
                                                                    <input placeholder="" type="text" name="variation[1][price][regular]" class="form-control">
                                                                </td>
                                                                <td>
                                                                    
                                                                     
                                                                    <input placeholder="" type="text" name="variation[1][price][sale]" class="form-control">
                                                                </td>

                                                                 <td>
                                                                    <!-- <div class="row">
                                                                        <div class="col-md-6"> -->
                                                                            
                                                                            <input type="date" name="variation[1][sale][start_date]" class="form-control">
                                                                        </td>
                                                                        <td>
                                                                       <!--  </div>
                                                                        <div class="col-md-6"> -->
                                                                            
                                                                            <input type="date" name="variation[1][sale][end_date]" class="form-control">
                                                                      <!--   </div>
                                                                    </div> -->
                                                                </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                
                                                            </tr>

                                                           
                                                        </table>
                                                    </div>


       

    </div>
    <div class="mb-4 ml-1 d-flex">
       <a href="/<?php echo $user->role;?>/module" style="margin-right:0.1rem">
            <button type="button" class="btn btn-dark wow zoomIn">{{__('academy.back')}} </button>
        </a>
        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('module.submit')}}</button>
  </div>
</form>
</div>

@endsection