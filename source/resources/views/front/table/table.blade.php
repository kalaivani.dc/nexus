<table id="target" class="table" style="width:100%">
    <thead class="thead-dark hidden">
        <tr>
            <th>

            <input class="data-check-num" type="checkbox" id="ship_all">
            <label class="data-check-num" for="ship_all">#</label>

            </th>

            <?php 

            foreach ($thead as $key => $value) {
            ?>
            <th <?php echo (isset($colspan) ? 'colspan='.$colspan : '');?>>{{__($value)}}</th>
            <?php
            }
            ?>

        </tr>
       
    </thead>
    <tbody></tbody>
</table>