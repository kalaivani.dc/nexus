<div id="portfolio-flters" class="btn-group mr-1 mb-1 d-sm-none">
    <div class="dropdown">
        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Menu
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <ul  data-aos="fade-up" data-aos-delay="100">
            <?php
            if (isset($keys)) {


            foreach ($keys as $b => $brand) {
            if (is_array($brand)) {
                ?>
                 <li data-filter=".filter-<?php echo (isset($brand['id'])? $brand['id']: $brand);?>" class="dropdown-item"><?php echo (isset($brand['name'])? $brand['name']: $brand);?></li>
                <?php
            } else {
                ?>
                <li data-filter=".filter-<?php echo (isset($brand->id)? $brand->id: $brand);?>" class="dropdown-item"><?php echo (isset($brand->name)? $brand->name: $brand);?></li>
                <?php

            }
            ?>

            
            <?php
            }
            }
            ?>
            </ul>
        </div>
    </div>
</div>