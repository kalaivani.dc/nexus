@extends('layouts.app2')


@section('styles')

@endsection

@section('content')
   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                        <div class="col-xl-3 col-lg-6  col-md-6 col-xxl-5 ">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade show active" id="first">
                                                <img class="img-fluid" src="https://nexustech.online/asset/front/images/product/1.jpg" alt="">
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="second">
                                                <img class="img-fluid" src="https://nexustech.online/asset/front/images/product/2.jpg" alt="">
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="third">
                                                <img class="img-fluid" src="https://nexustech.online/asset/front/images/product/3.jpg" alt="">
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="for">
                                                <img class="img-fluid" src="https://nexustech.online/asset/front/images/product/4.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="tab-slide-content new-arrival-product mb-4 mb-xl-0">
                                            <!-- Nav tabs -->
                                            <ul class="nav slide-item-list mt-3" role="tablist">
                                                <li role="presentation" class="show">
                                                    <a href="#first" role="tab" data-toggle="tab">
                                                        <img class="img-fluid" src="https://nexustech.online/asset/front/images/tab/1.jpg" alt="" width="50">
                                                    </a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#second" role="tab" data-toggle="tab"><img class="img-fluid" src="https://nexustech.online/asset/front/images/tab/2.jpg" alt="" width="50"></a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#third" role="tab" data-toggle="tab"><img class="img-fluid" src="https://nexustech.online/asset/front/images/tab/3.jpg" alt="" width="50"></a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#for" role="tab" data-toggle="tab"><img class="img-fluid" src="https://nexustech.online/asset/front/images/tab/4.jpg" alt="" width="50"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--content-->
                                    <div class="col-xl-9 col-lg-6  col-md-6 col-xxl-7 col-sm-12">
                                        <div class="product-detail-content">
                                            <!--Product details-->
                                            <div class="new-arrival-content pr">
                                                <h4 data-main-product-name id="<?php echo $product->id;?>"><?php echo $product->name;?></h4>
                                                <div class="comment-review star-rating">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star-half-empty"></i></li>
                                                        <li><i class="fa fa-star-half-empty"></i></li>
                                                    </ul>
                                                    <span class="review-text">(34 reviews) / </span><a class="product-review" href=""  data-toggle="modal" data-target="#reviewModal">Write a review?</a>
                                                </div>
                                     
                                               
                                                <p>Author: <span class="item">Lee</span> </p>
                                                
                                                <p>Tags:&nbsp;&nbsp;
                                                    <span class="badge badge-success light">bags</span>
                                                    <span class="badge badge-success light">clothes</span>
                                                    <span class="badge badge-success light">shoes</span>
                                                    <span class="badge badge-success light">dresses</span>
                                                </p>
                                               
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="row">
                                    <div class="col-12  mb-2">
                                        <?php
                                        echo nl2br($product->content);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                      
                 



@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('[data-variation]').on('change', function() {

            var regular = $(this).find('option:selected').data('price');
            var sale = $(this).find('option:selected').data('sale');
            var product_price = $('#pp').val();
            var product_sale = $('#ps').val();

            //if variant got regular price, display it
            if(regular != '0.00') {
                $('[data-product-price]').text('RM'+regular);

                //check if got sale also
                if(sale != '') {
                     //if got variant sale price, display the section
                     $('[data-pricing] p').removeClass('hidden');
                     //display the price
                     $('[data-pricing] [data-product-sale]').text('RM'+sale); 
                      //make the font bigger and color diff
                     $('[data-product-sale]').addClass('price');
                     //make the regular price smaller 
                     $('[data-product-price]').removeClass('price');
                     //and strike it
                     $('[data-product-price]').addClass('strike');


                } else {

                     //if no variant sale price, hide the part
                     $('[data-pricing] p').addClass('hidden');
                     //remove the strike from regular price, since no sale price
                     $('[data-product-price]').removeClass('strike');
                     //make regular price bigger
                     $('[data-product-price]').addClass('price');


                }

            } else {
                //else display main product price
                if(product_price != '') {
                    $('[data-product-price]').text('RM'+product_price);

                }

                //if main product got sale, diaply it too
                if(product_sale != '') {
                    //unhide sale section
                    $('[data-pricing] p').removeClass('hidden');
                    //display sale price
                    $('[data-pricing] [data-product-sale]').text('RM'+product_sale);  
                    //make the font bigger
                    $('[data-product-sale]').addClass('price');
                    //strike main product price
                    $('[data-product-price]').addClass('strike');
                    //make the font smaller
                    $('[data-product-price]').removeClass('price');

                }
            }
        });


        //add to cart
        var product = [];
        $('[data-cart-add]').on('click', function() {
           var product_id = $('[data-main-product-name]').attr('id');
           var quantity = $('#qty').val();

            if(jQuery.inArray(product_id, product) != -1) {
                var qty = $('[data-cart] #'+product_id+' input[name="product['+product_id+'][qty]"]').val();
                
                qty++;
                console.log(qty);
                $('[data-cart] #'+product_id+' input[name="product['+product_id+'][qty]"]').val(qty);
            } else {

                product.push(product_id);

                 $('[data-cart]').append('<li class="active" data-cart-item id="'+product_id+'">'+
                    '<div class="d-flex bd-highlight">'+
                        '<div class="user_info">'+
                            '<span><input class="form-control" type="text" name="product['+product+'][name]" value="'+$('[data-main-product-name]').text()+'"></span>'+
                            '<p><input class="form-control" type="text" data-product-qty name="product['+product+'][qty]" value="'+quantity+'"></p>'+
                        '</div>'+
                        '<div class="ml-auto">'+
                            '<a href="#" class="btn btn-primary btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>'+
                            '<a href="#" class="btn btn-danger btn-xs sharp" data-cart-item-remove><i class="fa fa-trash"></i></a>'+
                        '</div>'+
                    '</div>'+
                '</li>');
            }

           

            var item = $('[data-cart-item]').length;
            if(item > 0) {
                $('[data-cart-wrapper]').removeClass('hidden').text(item);

            }
    
        });

        //remove from cart
        $('body').on('click', '[data-cart-item-remove]', function() {

            $(this).parents('[data-cart-item]').remove();
            var item = $('[data-cart-item]').length;
            if(item > 0) {
                $('[data-cart-wrapper]').removeClass('hidden').text(item);

            } else {
                $('[data-cart-wrapper]').addClass('hidden');
            }
        });

        var category = '';
        var storeID = 0;
        $('#category').on('keypress', function(e) {
         
                if ($(this).val().length > 1) {
                  console.log($(this).val());
                  storeID = $('#storeid').val();

                  category = $(this).val();
                  /*fetch category*/
                          $.ajax({
                        url: "/category_post",
                        method: "POST",
                        data:{
                            "_token": "{{ csrf_token() }}",
                            'keyword':category,
                            'store':storeID,
                            'dataType':'html'
                        },
                        success: function (response) {
                            console.log(response);
                            $('#categorySuggestion').html(response);

                        }
                    });
                  /*fetch category*/

                }

                 if(e.which == 13) {
                alert('You pressed enter!');
            }
      });

    });
</script>

@endsection
