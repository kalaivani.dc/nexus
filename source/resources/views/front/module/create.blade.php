@extends('layouts.app')


@section('assets')

@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Add Module</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/module/main"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/module/main">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
    
    </div>
<form action="<?php echo $site;?><?php echo $user->role;?>/module?action=save" method="POST" enctype="multipart/form-data">@csrf
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-4">
                <div class="form-group">
                <label>{{__('module.name')}}</label>
                <input type="text" name="name" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <label>{{__('module.icon')}}</label>
                <input type="text" name="icon" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <label> {{__('module.parent')}}</label>
                <fieldset class="form-group">
                    <select name="parent" class="form-control">
                        <option value="">{{__('module.sparent')}}</option>
                            <?php
                                if (isset($modules)) {
                                    foreach ($modules as $key => $module) {
                            ?>
                            <option value="<?php echo $module->id;?>"><?php echo $module->name;?></option>
                            <?php
                            }
                                }
                            ?>
                    </select>
                </fieldset>
                </div>
            </div>
             <div class="col-md-4">
                <?php 
                if (isset($allroles)) {
                    foreach($allroles as $key=>$value) {
                        ?>
                        <input checked type="checkbox" name="role[]" value="<?php echo $value->id;?>"><?php echo $value->name;?>
                        <?php
                    }
                }
                ?>
             </div>
          
        </div>

        


       

    </div>
    <div class="mb-4 ml-1 d-flex">
       <a href="/<?php echo $user->role;?>/module" style="margin-right:0.1rem">
            <button type="button" class="btn btn-dark wow zoomIn">{{__('academy.back')}} </button>
        </a>
        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('module.submit')}}</button>
  </div>
</form>
</div>

@endsection