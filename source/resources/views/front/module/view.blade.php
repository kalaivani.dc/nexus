@extends('layouts.app')

@section('styles')

@endsection

@section('content')

<ul>
<?php 

foreach ($modules as $key => $value) {
   ?>
   <li>
        <?php 
        if (isset($value['name'])) {
            ?>
            <a href="<?php echo $site.str_replace('{{role}}', $user->role, $value['url']);?>"><?php echo $value['name'];?> </a>
            <?php
        }
        ?>
            <?php
            if(isset($value['children'])) {
                ?>
                <ul>
                <?php
                foreach ($value['children'] as $k => $v) {
                   ?>
                   <li>
                    <a href="<?php echo $site.str_replace('{{role}}', $user->role, $v['url']);?>"><?php echo $v['name'];?></a>
                        <?php
                        if(isset($v)) {
                            ?>
                            <ul>
                            <?php
                            foreach ($v[$v['id']] as $k2 => $v2) {
                               ?>
                               <li>
                                <a href="<?php echo $site.str_replace('{{role}}', $user->role, $v2['url']);?>"><?php echo $v2['name'];?></a>
                                 <?php
                                    if(isset($v2)) {
                                        ?>
                                        <ul>
                                        <?php
                                        foreach ($v2[$v2['id']] as $k3 => $v3) {
                                           ?>
                                           <li><a href="<?php echo $site.str_replace('{{role}}', $user->role, $v3['url']);?>"><?php echo $v3['name'];?></a></li>
                                           <?php
                                        }
                                        ?>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                    
                                </li>
                               <?php
                            }
                            ?>
                            </ul>
                            <?php
                        }
                        ?>
                    </li>
                   <?php
                }
                ?>
                </ul>
                <?php
            }
            ?>
   </li>
   <?php
}
?>
</ul>
@endsection

@section('scripts')

@endsection
