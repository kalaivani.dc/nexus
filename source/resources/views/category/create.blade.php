@extends('layouts.app')


@section('assets')
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0 d-none d-sm-block">Add Category</h5>
            <h5 class="content-header-title pr-1 mb-0 d-block d-sm-none"> {{__('module.edit')}}</h5>
                <div class="breadcrumb-wrapper d-none d-sm-block">
                    <ol class="breadcrumb p-0 mb-0 pl-1">
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role?>/home"><i class="bx bx-home-alt"></i></a> </li>
                        <li class="breadcrumb-item"><a href="/<?php echo $user->role;?>/home">{{__('module.module')}}</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header text-left">
    
    </div>
<form action="/backend/<?php echo $user->role;?>/category?action=save" method="POST" enctype="multipart/form-data">@csrf
    <div class="card-body">
        <div class="row col-12">
            <div class="col-md-3">
                <div class="form-group">
                <label>Product Name</label>
                <input type="text" name="name" class="form-control">
                </div>
            </div>


             <div class="col-md-3">

                
                <label>Status</label>
                <input type="radio" class="form-control" name="status" value="1">Active
                <input type="radio" class="form-control" name="status" value="0">Inactive
                
             </div>

             <div class="col-md-3">
                <div class="form-label-group position-relative has-icon-left">
                    <input type="text" id="category" class="form-control" name="fname-floating-icon" placeholder="Category" data-category>
                    <input type="button" id="categorySearch" value="Search">
                    <div class="form-control-position">
                        <i class="bx bx-user"></i>
                    </div>
                    <label for="category">Parent</label>
                </div>

                <div class="col-12">
                    <div id="categorySuggestion"></div>
                </div>
            </div>
            
            
          
        </div>
        
        <div class="row col-12">
            <div class="col-md-12">
                <div class="form-label-group position-relative has-icon-left">
                    <input type="file" name="featured_pic"  id="email-id-floating-icon" class="form-control" name="email-id-floating-icon" placeholder="Featured Image">
                    <div class="form-control-position">
                        <i class='bx bx-photo-album'></i>
                    </div>
                    <label for="email-id-floating-icon">Featured Image</label>
                </div>
            </div>
            
        </div>
    
    </div>
    
    
                                            

    <!--description-->
       <div class="col-12">
        <div class="card">

         <div class="card-body">
             <textarea name="content" id="content" class="hidden"></textarea>
                       <section class="full-editor">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Description</h4>
                                </div>
                                <div class="card-body">
                                  
                                    <div class="row">
                                      <div class="col-sm-12">
                                            <div id="full-wrapper">
                                                <div id="full-container">
                                                    <div class="editor">
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                                </div>
                            </div>
                        </div>

                    

    <div class="mb-4 ml-1 d-flex">

        <button type="submit" class="btn btn-primary subtotal-preview-btn" data-submit>{{__('submit')}}</button>
  </div>
</form>
</div>

@endsection
@section('scripts')

<script type="text/javascript">

$(function() {

       //variation
    $("body").on("click", "[data-add-product]", function() {
    
        var $trLast =  $($(this).parents('[data-product_wrapper]'));
        var id = $('[data-product_wrapper]').length;
        id++;

        //get total attribute to determine id
         var attrID = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;


        $trNew = $trLast.clone();
        $trNew.find('tr').attr('id', id);

        //variation id
        $($trNew).find('tr [data-number]').text(id).addClass('badge badge-circle badge-secondary');

        

        /**renumber product element**/
        $($trNew).find('tr th [data-product-name]').attr('name', 'variation['+id+'][variation]');


        //renumber copied attributes
        var rno = 0;
        $.each($($trNew).find('.variation_wrapper2 [data-variant-wrapper]'), function(key, value) {
           rno++;

           $(value).find('td input[data-variation-value]').attr('name', 'variation['+id+']['+rno+'][value]');
           $(value).find('td input[data-regular-price]').attr('name', 'variation['+id+']['+rno+'][price][regular]');
           $(value).find('td input[data-sale-price]').attr('name', 'variation['+id+']['+rno+'][price][sale]');
           $(value).find('td input[data-sale-start]').attr('name', 'variation['+id+']['+rno+'][sale][start_date]');
           $(value).find('td input[data-sale-end]').attr('name', 'variation['+id+']['+rno+'][sale][end_date]');
        });

        $($trNew).insertBefore($trLast);


  });
     //attributes
    $("body").on("click", "[data-add-productspecs]", function() {
        var id = $(this).parents('[data-product_wrapper]').find('tr').attr('id');

        var $trLast =  $($(this).parents('.variation_wrapper2')).find('table tr:last');
        $trNew = $trLast.clone();

        $trLast.css('background', 'yellow');

         //get total attribute to determine id
        var rno = $($(this).parents('[data-product_wrapper]')).find('.variation_wrapper2 [data-variant-wrapper]').length;

        rno++;

        $.each($($trNew), function(key, value) {
           
            $(value).find('td input[data-variation-value]').attr('name', 'variation['+id+']['+rno+'][value]');
           $(value).find('td input[data-regular-price]').attr('name', 'variation['+id+']['+rno+'][price][regular]');
           $(value).find('td input[data-sale-price]').attr('name', 'variation['+id+']['+rno+'][price][sale]');
           $(value).find('td input[data-sale-start]').attr('name', 'variation['+id+']['+rno+'][sale][start_date]');
           $(value).find('td input[data-sale-end]').attr('name', 'variation['+id+']['+rno+'][sale][end_date]');


        });
        
        $trLast.after($trNew);
     });


    //save product
    $('body').on('click', '[data-submit]', function(e) {
     
         e.preventDefault = false;
        var status = false;
        if (status == false) {

            var quill = new Quill ('.editor');
            var quillHtml = quill.root.innerHTML.trim();
            $('#content').val(quillHtml);
            e.preventDefault = false;
        }

      
    });

    $('#categorySearch').on('click', function(e) {
        var keyword = $('#category').val();
        console.log(keyword);
         $.ajax({
            url: "/backend/admin/category?action=list",
            method: "POST",
            data:{
                "_token": "{{ csrf_token() }}",
                'keyword':keyword,
                'dataType':'html'
            },
            success: function (response) {
                console.log(response);
                $('#categorySuggestion').html(response);

            }
        });

    });

    $("body").on('click', "[data-delete-product]", function() {
            
        $($(this).parents('[data-product_wrapper]')).remove();
   });

   $("body").on('click', "[data-delete-variant]", function() {

        $($(this).parents('[data-variant-wrapper]')).remove();
   });



});
</script>
@endsection