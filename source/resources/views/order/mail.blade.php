<!doctype html>
<html lang="en">
  <head>
    <title>Axis Billing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  </head>
  <body>
       <table style="width:80%;margin:0 auto;">

                        <tr>
                            <th style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">Invoice</th> 
                            <th style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"><strong><?php echo $order->created_date;?></strong> </th>
                            <th style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"><span class="float-right"><strong>Status:</strong> <?php echo $order->status;?></span></th>
                        </tr>
                           
                               
                        <tr>
                            <th style="text-align:left;color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">
                            <h6 style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">From:</h6>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"> <strong>PREMIO PRINTDEPOT SDN BHD</strong> </div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">MR. JOEY WONG</div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">1st & 2nd Flr,No 1, Jalan SS 6/5B,</div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">Dataran Glomac, Kelana Jaya,</div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">47301 Petaling Jaya, Selangor Dahrul Ehsan</div>
                            <br/>
                        </th>
                        <th style="text-align:left;color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">
                            <h6 style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">To:</h6>
                            
                            
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"> <strong>Attn: <?php echo $order->firstName;?> <?php echo $order->lastName;?></strong> </div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"><?php echo $order->address;?></div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"><?php echo $order->address2;?>, <?php echo $order->zip;?>, <?php echo $order->state;?></div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;"><?php echo $order->country;?></div>
                            <div style="color:#727E8C;font-family: 'Rubik', Helvetica, Arial, serif;">Email: <?php echo $order->username;?></div>
                             <br/>
                       </th>
                       <th style="text-align:right;padding:1.15rem 2rem;color:#727E8C;" colspan="4">
                           	<img class="logo-abbr mr-2" src="http://online.printdepot.com.my/asset/home/assets/img/logo-207x41.png" alt="">
                        </th>
                        </tr>
               </table>
               <table style="width:80%;margin:0 auto;">
                        <tr>
                            <th style="text-align:left;width:20px;padding:1.15rem 2rem;color:#727E8C;">#</th>
                            <th style="text-align:left;width:300px;padding:1.15rem 2rem;color:#727E8C;">Item</th>
                            <th style="text-align:left;width:300px;padding:1.15rem 2rem;color:#727E8C;">Description</th>
                            <th style="text-align:right;padding:1.15rem 2rem;color:#727E8C;">Unit Cost(RM)</th>
                            <th style="text-align:right;width:50px;padding:1.15rem 2rem;color:#727E8C;">Qty</th>
                            <th style="text-align:right;padding:1.15rem 2rem;color:#727E8C;">Total(RM)</th>
                        </tr>
               
               
                        <?php
                        if(isset($detail)) {
                            $count = 0;
                            foreach($detail['product'] as $key=>$value) {
                                $count++;
                                
                               ?>
                               <tr style="<?php echo ($count % 2 == 0 ?'' :'background-color: rgba(0,0,0,.05)');?>">
                                    <td style="text-align:left;width:20px;padding:1.15rem 2rem;color:#727E8C;"><?php echo $count;?></td>
                                    <td style="text-align:left;width:300px;padding:1.15rem 2rem;color:#727E8C;"><?php echo $value->name;?></td>
                                    <td  style="text-align:left;width:300px;padding:1.15rem 2rem;color:#727E8C;"></td>
                                    <td style="text-align:right;padding:1.15rem 2rem;color:#727E8C;"><?php echo $value->sub_total;?></td>
                                    <td style="text-align:right;width:50px;padding:1.15rem 2rem;color:#727E8C;"><?php echo $value->quantity;?></td>
                                    <td style="text-align:right;padding:1.15rem 2rem;color:#727E8C;"><?php echo $value->total;?></td>
                                </tr>
                                
                               <?php
                               
                               if(isset($detail['variant'][$value->product_id])) {
                                            ?>
                                          
                                            <?php
                                            $i=0;
                                           foreach($detail['variant'][$value->product_id] as $k=>$v) {
                                            $i++;
                                               ?>
                                               <tr style="<?php echo ($i % 2 == 0 ?'' :'background-color: rgba(0,0,0,.05)');?>">
                                                   <td></td>
                                                   <td style="padding:1.15rem 2rem;color:#727E8C;"><span class="bullet bullet-success bullet-sm"></span> <?php echo $v->variant_type;?></td>
                                                   <td style="padding:1.15rem 2rem;color:#727E8C;text-align:left;"><?php echo $v->variant_value;?></td>
                                                   <td style="padding:1.15rem 2rem;color:#727E8C;text-align:right;"><?php echo $v->variant_price;?></td>
                                                   
                                               </tr>
                                               <?php
                                           }
                                           ?>
                                           
                                           <?php
                                       }
                            }
                            
                           
                                       
                                       
                                      
                        }
                        
                        ?>
                                            
                                           
                                        
                                   
                               
                        <tr>
                            <td colspan="6" style="text-align:right;padding:1.15rem 2rem;color:#727E8C;"><b>Total RM<?php echo $order->grand_total;?></b><br>
                        </tr>
                
               
           </table>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
