<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConfigController as Config;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('sendbasicemail',[App\Http\Controllers\MailController::class, 'basic_email']);
Route::get('sendhtmlemail',[App\Http\Controllers\MailController::class, 'html_email']);
Route::get('sendattachmentemail',[App\Http\Controllers\MailController::class, 'attachment_email']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::post('updatewallet', 'ShopController@updatewallet');
// Route::post('productsByCat', 'ShopController@getProducts')->name('product');


//bootstrap
Route::middleware(['auth'])->group(function () {

    Route::prefix('backend/{role}')->group(function () {

        /*initialize config*/
        $config =  new Config();
        $tier = '';

        for($i = 0; $i < 3; $i++)
        {
            $tier .= "{tier_$i}/";
  
            Route::prefix($tier)->group(function () {
                routerCallback();
                   
            });

        }

    });

});

    //front end
    /*initialize config*/
    $config =  new Config();
    $tier = '';

    for($i = 0; $i < 3; $i++)
    {
        $tier .= "{tier_$i}/";

        Route::prefix($tier)->group(function () {
            guestRouterCallback();
               
        });

    }


function routerCallback()
{

    Route::get('/', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::post('/', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::get('q={id}', [App\Http\Controllers\RouterController::class, 'getRouter']);

    Route::get('action={action}', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::get('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\RouterController::class, 'getRouter']);

    Route::post('action={action}', [App\Http\Controllers\RouterController::class, 'getRouter']);
    Route::post('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\RouterController::class, 'getRouter']);
}


function guestRouterCallback()
{

    Route::get('/', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::post('/', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::get('q={id}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);

    Route::get('action={action}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::get('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);

    Route::post('action={action}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
    Route::post('action={action}&q={id}&r={id2}&s={id3}', [App\Http\Controllers\GuestRouterController::class, 'getRouter']);
}