<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
   public function blog(Request $request, $internal = false, $guest = false)
    {

        $extra = '';
        $id = $request->query('id');
        $input = $request->all();
        $backend = $request->query('backend');
        $cat = 0;

        if(!empty($id)) {
             //get category
            $query="SELECT * FROM blog WHERE id = $id";
            $category = DB::select($query);

            $cat = $category[0]->id;


            return $this->view('front.blog.blog',
                    [   
                        'guest'=>$guest,
                        'skipHeader'=>1,
                        'product'=>$category[0],
                        'save'=>'https://nexustech.online/blog']);

        }
        
        if (!$guest) {

            $user = new UserController(); 
            $role = $user->loggedInUser();
        

         $add = '/backend'.$this->site.$role->role.'/blog?action=add';
        }

         if (!$guest) {

            $view = 'blog.view';
        } else {
            $view = 'front.blog.view';

        }


          if (!$guest) {
            $thead = ['name', 'slug', 'excerpt', 'author', 'created at'];
            $add_title = 'ADD BLOG';
            $keys = ['all'];
            $search = 1;
         } else {
            $thead = ['name'];
            $add_title='';
             $search = 0;
            $keys = ['all'];
             
         }

         
         return $this->view($view,
        [
            'add'=>(isset($add)? $add: ''),
            'guest'=>$guest,
            'main_title'=>'Blog',
            'bread'=>[['#'=>'home'], ['#'=>'blog']],
            'add_title'=>$add_title,
            'cat'=>$cat,
            'vendor'=>[],
            'thead'=>$thead,
            'search'=>$search,
            'keys'=>$keys]);
    }


     //using ajax
     public function blog_post(Request $request, $internal = false)
    {
        $input = $request->all();

        $extra = '';
        $input = $request->all();

        $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

         if (isset($input['id']) && !empty($input['id'])) {

            $extra.= " AND c.id = ".$input['id'];

        }

        
         $query = "";
   
        $query="SELECT [param] FROM blog c LEFT JOIN users u ON u.id = c.user_id WHERE 1 = 1 $extra";

        $data = $this->paginate($query, 'c.*, u.name as author', $this->offset, $this->limit);
          

        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            if ($input['guest'] == 1) {
             $column['all'][] = $this->setGridBlog(
                            $value->id,[
                            '<a  href="?id='.$value->id.'">'.$value->name.'</a>',
                            $value->excerpt
                            ], $data['rowCount'], $value->id);
         } else {
             $column['all'][] = $this->setRow(
                            $value->id,[
                            '<a class="linkcolor" href="?id='.$value->id.'">'.$value->name.'</a>',
                            $value->slug,
                            $value->excerpt,
                            $value->author,
                            $value->created_at
                            ], $data['rowCount'], $value->id);

         }


         }


     
    if ($input['guest'] == 1) {

         $column2 = [];

         foreach ($column as $key => $value) {
                $total = sizeof($value);
                $chunk = array_chunk($value, 3);
           
                foreach ($chunk as $k => $v) {

                $tr = '<tr class=" parent-row">';

                    foreach ($v as $k2 => $v2) {
                        
                        $tr .=$v2;
                    }
                $tr .='</tr>';
                $column2[$key][] = $tr;


                }
           
         }
}
  
         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

        
        if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');
        
        if ($input['guest'] == 1) {
        $data['data'] = $column2;

        } else {
        $data['data'] = $column;

        }

        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

      return $data;
    }

   
    public function blogAdd()
    {
        $user = new UserController(); 
        $role = $user->loggedInUser();

        return $this->view('blog.create', [
        'title'=>'Blog',
        'user'=>$role

      ]);
    }

    public function blogSave(Request $request)
    {
        $input = $request->all();

        $filePath = 'images/blogs/'.Auth::id();
        $string = preg_replace('/\s+/', '-', $input['name']);
        $slug = strtolower($string);

        $ebay = new \App\Models\Blog;
        $ebay->name = $input['name'];
        $ebay->slug = $slug;

        $ebay->excerpt = (isset($input['excerpt'])? $input['excerpt'] : '');
        $ebay->content = $input['content'];
        // $ebay->picture = $input['featured'];
        // $ebay->featured_copyright = $input['featured_copyright'];

        
        $ebay->publish = 1;//($input['publish'] == 'on' ? 1 : 0);

        $ebay->user_id = Auth::id();
        
        $path = public_path($filePath);
   
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path, 0777, true, true);
        }

        //profile pic
        $profilePath = public_path($filePath);
   
        if(!\File::isDirectory($profilePath)){
            \File::makeDirectory($profilePath, 0777, true, true);
        }

        if ($request->file('featured') != null) {
            //upload picture
           $request->validate([
                'featured' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:9500',
            ]);

            $file = $request->file('featured')->getClientOriginalName();

            $filename = pathinfo($file, PATHINFO_FILENAME);
            $extension = pathinfo($file, PATHINFO_EXTENSION);

            $profileName = time().$filename.'.'.$request->featured->extension();  

            $request->featured->move(public_path($filePath), $profileName);

            $ebay->picture = $filePath.'/'.$profileName;
        }
        $ebay->save();

        return redirect('blog'.DIRECTORY_SEPARATOR.$slug.DIRECTORY_SEPARATOR);


    }
}
