<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Jobs\ProcessRequests;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Facades\Http;
use App\Events\MessageTransport;


class ShopController extends Controller
{
     public function updatewallet(Request $request)
    {
        $input = $request->all();
        //get wallet and assign user id
        
        event(new MessageTransport('data', ['id'=>17, 'data'=>$input]));
        exit;
    }
    public function getProducts(Request $request)
    {
        $input = $request->all();

    }

    public function shop(Request $request, $internal = false)
    {
        $extra = '';
        $id = $request->query('id');
        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
        $query = "SELECT * FROM shop $extra";
        $shops = DB::select($query);
        if($internal) {
            return $shops;
        }

         return $this->view('shop.view',
        [
            'shops'=>$shops]);
    }

     public function shopAdd(Request $request)
    {
        $logistic = new UserController($request);
        $role = new RoleController($request);
        $allRoles = $role->getRole();

        $roles = $logistic->getUserRoles();


        return $this->view('shop.create',
        [
            'roles'=>$roles]);
    }

     public function ShopSave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        $query="INSERT INTO shop(name, created_by)VALUES('".addslashes($input['name'])."', $userID)";
        $insert = DB::insert($query);

        return redirect()->back()->with('success', 'Shop Created');


    }

     



    
   

    public function package(Request $request)
    {
    }
}
