<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use \App\Http\Controllers\UserController as UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \App\Http\Controllers\RouterController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use \App\Http\Controllers\HelperController;



    public $site;
    public $user;
    public $guest;


    public function __construct() {
        $this->site = '/';
    }

    public function getUser()
    {

        if (!isset($this->user)) {

            $user = new UserController();
            $this->user = $user->loggedInUser();
        }

    }
    public function view($page, $param) {

         $user = new UserController();
         $role = $user->loggedInUser();
   
        if ($role) {

            $user = new UserController();
            $role = $user->loggedInUser();
            $param['menu'] = $this->menu($role->id, $role->role_id, 0);
            $param['cats'] = $this->cats($role->id, $role->role_id, 0);
        } else {
            $param['menu'] = $this->menu('5', 'admin', 1);
            $param['cats'] = $this->cats('5', 'admin', 1);
        }

        
        $param['site'] = '/';

       return view($page,  $param);
    }
    public function menu($user, $role, $guest)
    {
        ob_start();
        $this->showCategoryTree(1, $user, $role, $guest);
        $menuHtml = ob_get_contents();
        ob_end_clean();
        return $menuHtml;
    }
    public function cats($user, $role, $guest)
    {
        ob_start();
        $this->showProductCategoryTree(1, $user, $role, $guest);
        $menuHtml = ob_get_contents();
        ob_end_clean();
        return $menuHtml;
    }
}
