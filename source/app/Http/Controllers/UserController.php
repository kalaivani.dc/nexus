<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\UserDetail;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
        $extra = '';
        $id = $request->query('id');
        $input = $request->all();

          if(!empty($id)) {
   
            $query="SELECT * FROM orders WHERE id = $id";
            $order = DB::select($query);

            return $this->view('user.user',
                    [   
                        'skipHeader'=>1,
                        'order'=>$order[0],
                        'save'=>'https://nexustech.online/blog']);

        }

         return $this->view('user.view',
        [
            'thead'=>['name', 'email', 'role', 'created at'],
            'keys'=>['all']]);
    }

    //using ajax
    public function user_post(Request $request)
    {
        $input = $request->all();
        $query="SELECT * FROM orders";
        $orders = DB::select($query);

         $this->limit = 100;
         $offsetStr = '';
         $extra = "";
         if (isset($input['offset'])) {
            $this->offset = $input['offset']; 
         } else {
            $this->offset = 0;
         }

        $query="SELECT [param] FROM users o";
        $data = $this->paginate($query, 'o.*', $this->offset, $this->limit);

        foreach ($data['result'] as $key => $value)
         {
            $data['rowCount']++;
            $column['all'][] = $this->setRow(
                $value->id,[
                '<a class="linkcolor" href="?id='.$value->id.'">'.$value->name.'</a>',
                $value->email,
                $value->role,
                $value->created_at
                ], $data['rowCount'], $value->id);

        }

         $totalkeys = array_keys($column);
         $rowCount = [];
         //count rows
         foreach ($totalkeys as $key => $value) {
            $rowCount[$value] = sizeof($column[$value]);
         }

         if (isset($input['paginate'])) {
            $data['paginate'] = (isset($input['paginate'])? $input['paginate'] : 0);
        }

        (isset($input['search'])? $data['search'] = $input['search']: '');

        $data['data'] = $column;
        $data['fresh'] = (isset($input['fresh'])? $input['fresh'] : 0);
        $data['totalRows'] = $data['totalRows'];
        $data['more'] = $data['more'];
        $data['offset'] = $data['offset'];
        $data['totalRows'] = $data['totalRows'];
        $data['rowCountByKey'] = $rowCount;


        unset($data['result']);

        return $data;
    }
     public function getusers($all = false)
   {
        // if (!$all) {
        //     $extras = "WHERE ud.user_id = ".Auth::id();
        // }
 
        $role = $this->loggedInUser();


        //show only for the user
        if ($role->role !== 'admin') {

            $extras = "WHERE ud.user_id = ".Auth::id();
        } else if ($role->role !== 'staff'){
            $extras = "WHERE ud.user_id = ".Auth::id();

        } else {
            $extras = '';
        }

       $query="SELECT DISTINCT u.name, ud.user_id, ud.* FROM users u INNER JOIN axis_user_detail ud ON u.id = ud.user_id $extras";

 
       $users = DB::select($query);

       return $users;
   }

   public function getUserRoles()
   {
        $query = "SELECT * FROM roles";
        $roles = DB::select($query);
        return $roles;
   }
    public function loggedInUser($role = false, $userID = false)
    {

        if (!$role) {
            $role = \Route::current()->parameter('role');
        }

        if (!$userID) {
            $userID = Auth::id();
        }
       
        if ($userID) {
             $query = "SELECT u.id, u.name, r.name as role, r.id as role_id  FROM users u INNER JOIN roles r ON u.role = r.id  WHERE u.id = $userID";

                $user = DB::select($query);
        
                return $user[0];
        } else {
            return false;
        }
       

    }
}
