<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use \App\Http\Controllers\UserController as UserController;
use \App\Http\Controllers\ReflectionClass;



class ModuleController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        echo "<pre>";
        var_dump($request->all());
    }

    public function one()
    {
        echo "one";
    }

    public function oneSave()
    {
        echo "one save";
    }

    public function getModuleId($module)
    {
        $query="SELECT id FROM module WHERE name = '".$module."'";
        $id = DB::select($query);
        return $id[0]->id;
    }

    public function getParentModules($id)
    {
        $parents = [];

        static $parents;

        if ($id != null) {
            //check for parents
            $query="SELECT parent, name FROM module WHERE id = ".$id;

            $db = DB::select($query);

            $parents[] = ['id'=> $db[0]->parent, 'name'=>$db[0]->name];
            $this->getParentModules($parents[sizeof($parents)-1]['id']);

            return $parents;

        }
    }

    private function formModule($modules)
    {
        $output = [];

    
        foreach($modules as $module) {
            
            //3 level nested children
            $children = $this->getChildModules($module->id);
            foreach ($children as $key => $value) {

                $children[$key][$value['id']] = $this->getChildModules($value['id']);
                foreach($children[$key][$value['id']] as $k=>$v) {
                    $children[$key][$value['id']][$k][$v['id']] = $this->getChildModules($v['id']);


                }

            }

            $output[$module->id] = [
                'id'=>$module->id,
                'name'=>$module->name,
                'url'=>$module->url,
                'status'=>$module->status,
                'icon'=>$module->icon,
                'children'=>$children
            ];

       }

       return $output;
    }

    public function karen2()
    {
        echo "here";
    }

    private function getChildModules($id)
    {
        $children = [];

        if ($id != null) {

            //check for children
            $query="SELECT id, name, url, status, icon FROM module WHERE parent = ".$id;

            $db = DB::select($query);
            if (isset($db[0])) {
                  
                foreach ($db as $key => $value) {
                    $children[] = ['id'=> $value->id, 'name'=>$value->name, 'url'=>$value->url, 'status'=>$value->status, 'icon'=>$value->icon];

                }
            }

            return $children;

        }
    }

    private function formModulePath($id, $module)
    {
        $sub = [];
        $path = '';
        $parents = $this->getParentModules($id);

        if ($parents != null) {
            foreach ($parents as $key => $value) {

                $sub[] = $value['name'];

            }
            
            $sub = array_reverse($sub);
            $path = "{{role}}/".implode('/', $sub).'/'.$module;

        }

        return $path;
    }
    public function ModuleSave(Request $request)
    {
        $input = $request->all();

        $module = str_replace(' ', '_', strtolower($input['name']));
        $path = $this->formModulePath($input['parent'], $module);
        
        //create module
        $query = "INSERT INTO module(name, url, icon, parent)VALUES('".$module."','".$path."','".$input['icon']."', ".($input['parent'] != null ? $input['parent']: 0).")";
        $insert = DB::insert($query);
        $moduleID = DB::connection()->getPdo()->lastInsertId();

        if (isset($input['role'])) {
            foreach ($input['role'] as $key => $role) {
               
                //assign role access
                 $query = "INSERT INTO module_access_role(module_id, role_id)VALUES('".$moduleID."', '".$role."')";
                 $insert = DB::insert($query);
            }
        }

        


        return redirect()->back()->with('success', 'Module Created');
    }

    

     public function module($internal = false)
    {

        $query = "SELECT * FROM module WHERE parent = 0";
        $modules = DB::select($query);
        $output = $this->formModule($modules);

        return $this->view('module.view',
        [
            'modules'=>$output]);

    }
    private function actions()
    {
        //get those with module prefix
        $methods = get_class_methods($this);
        $class = new \ReflectionClass($this);
        $methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
             

         foreach ($methods as $method) {

            if (strpos($method->name, strtolower('module')) === 0) {
                print $method->name."<br>";

            }
         }
    }

     public function moduleAdd(Request $request)
    {
        $logistic = new UserController($request);
        $role = new RoleController($request);
        $allRoles = $role->getRole();

        $roles = $logistic->getUserRoles();
        $query = "SELECT * FROM  module WHERE status = 1 ";
        $modules = DB::select($query);

        $query = "SELECT * FROM actions";
        $actions = DB::select($query);

        return $this->view('module.create',
        [
            'roles'=>$roles,
            'modules'=>$modules,
            'allroles'=>$allRoles,
            'actions'=>$actions]);
    }

     public function moduleEdit(Request $request)
    {
        $id = $request->query('q');
        $query="SELECT * FROM module WHERE id = $id";
        $module = DB::select($query);

        $query="SELECT * FROM module WHERE parent = $id";
        $children = DB::select($query);

        return $this->view('module.edit',
            ['id'=>$id,
            'module'=>(array)$module[0],
            'children'=>(sizeof($children)> 0 ? $children:0)]);

    }

    public function moduleUpdatePost(Request $request)
    {
        $id = $request->query('q');
        $input = $request->all();
        
        $query = "UPDATE module SET name='".$input['name']."', url='".$input['path']."',status=".$input['status']." WHERE id= $id";

        $children = DB::update($query);

        if (isset($input['menu'])) {

            foreach($input['menu'] as $key => $menu) {
                $query = "UPDATE module SET name = '$menu' WHERE id = $key";
           
                $children = DB::update($query);


            }
        }

        return redirect()->back()->with('success', "Module Successfully Updated");

    }

    public function moduleDelete(Request $request)
    {
        $id = $request->query('q');
        $query="DELETE FROM module WHERE id = $id";
        $modules = DB::delete($query);

        return redirect()->back()->with('error', "Module Successfully Deleted");

    }

      public function read($internal = false, $user = false, $role = false)
    {
        $query = "SELECT m.*, mcr.action_id FROM module m INNER JOIN module_access_user mcr ON m.id = mcr.module_id WHERE m.status = 1 AND mcr.user_id = $user AND m.backend = 1 ORDER BY position ASC";

        $modules = DB::select($query);


        $output = [];

       foreach($modules as $module) {
        
        if ($module->parent == 0) {
            $output[$module->id] = [
                'id'=>$module->id,
                'name'=>$module->name,
                'url'=>$module->url,
                'status'=>$module->status,
                'icon'=>$module->icon
            ];
        }
       }
       foreach($modules as $module) {
        
        if ($module->parent != 0) {

            if (in_array($module->parent, array_keys($output))) {

                $output[$module->parent]['children'][] = [
                    'id'=>$module->id,
                    'name'=>$module->name,
                    'url'=>$module->url,
                    'status'=>$module->status,
                    'parent'=>$module->parent
                ];
            } else {
                $children = array_column($output, 'children');

                   foreach ($children as $key => $value) {

                    if(!is_bool(array_search($module->parent, array_column($value, 'id')))) {

                       
                        $index = array_search($module->parent, array_column($value, 'id'));
                        $traceParent = $value[$index]['parent'];

                        
        

                       
                        $output[$traceParent]['children'][$index]['grand-child-1'][] = [
                            'id'=>$module->id,
                            'name'=>$module->name,
                            'url'=>$module->url,
                            'status'=>$module->status,
                            'parent'=>$module->parent
                        ];
                    }
                        
                   }
                  
                 
            }
        }
       }

      
       if (is_int($internal) && $internal == 1) {
        return $output;
       }

        return $this->view('module.view',
        [
            'modules'=>$output]);

    }
    
      public function cats($internal = false, $user = false, $role = false)
    {
        $query = "SELECT m.* FROM category m WHERE m.status = 1 ORDER BY position ASC";

        $modules = DB::select($query);


        $output = [];

  
       foreach($modules as $module) {
        
        if ($module->parent == 0) {
            $output[$module->id] = [
                'id'=>$module->id,
                'name'=>$module->name,
                'url'=>$module->url,
                'status'=>$module->status,
                'icon'=>$module->icon
            ];
        } 
       
   
       }
       
       foreach($modules as $module) {
        
        if ($module->parent != 0) {
            
            $multicats = explode(',', $module->parent);
            foreach($multicats as $a=>$b) {
                
           
            if (in_array($b, array_keys($output))) {

                $output[$b]['children'][] = [
                    'id'=>$module->id,
                    'name'=>$module->name,
                    'url'=>$module->url,
                    'status'=>$module->status,
                    'parent'=>$b
                ];
            } else {
                $children = array_column($output, 'children');

                   foreach ($children as $key => $value) {

                    if(!is_bool(array_search($b, array_column($value, 'id')))) {

                       
                        $index = array_search($b, array_column($value, 'id'));
                        $traceParent = $value[$index]['parent'];

                        
        

                       
                        $output[$traceParent]['children'][$index]['grand-child-1'][] = [
                            'id'=>$module->id,
                            'name'=>$module->name,
                            'url'=>$module->url,
                            'status'=>$module->status,
                            'parent'=>$b
                        ];
                    }
                        
                   }
                  
                 
            }
            }
        }
       }

      
       if (is_int($internal) && $internal == 1) {
        return $output;
       }

        return $this->view('module.view',
        [
            'modules'=>$output]);

    }



}
