<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{
   public function vendor(Request $request, $internal = false, $guest = false)
    {
        
        $extra = '';
        $id = $request->query('id');
        if ($id > 0) {
            $extra.= "WHERE id = $id";
        }
        $query = "SELECT * FROM vendor $extra";
        $vendor = DB::select($query);

        
        if($internal) {
            return $vendor;
        }

         return $this->view('vendor.view',
        [
            'vendor'=>$vendor]);
    }
    public function vendorAdd(Request $request)
    {
        $logistic = new UserController($request);
        $role = new RoleController($request);
        $shops = $this->shop($request, 1);

        return $this->view('vendor.create',
        [
            'shops'=>$shops]);
    }

     public function vendorSave(Request $request)
    {
        $input = $request->all();
        $userID = Auth::id();
        $query="INSERT INTO vendor(name, shop_id, created_by)VALUES('".addslashes($input['name'])."', '".implode(',', $input['shop'])."',$userID)";
        $insert = DB::insert($query);

        return redirect()->back()->with('success', 'Vendor Created');


    }
}
